from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from medicine.models import Action


@csrf_exempt
def actions_count(request):
    count = Action.objects.filter(institution=request.GET.get('institution_id')).count()
    return JsonResponse({'status': 'success', 'count': count})
