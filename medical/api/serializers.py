# -*- coding: utf-8 -*-
from rest_framework import serializers
from django.conf import settings

from medicine.models import Institution, Doctor, Category, Comment, Photo, Phone, Action, Service, Price, Specialty, PushUp, Banner


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        exclude = ('title_kk', 'title_ru')


class PriceSerializer(serializers.ModelSerializer):
    institution_name = serializers.ReadOnlyField(source='institution.name', read_only=True)

    class Meta:
        model = Price
        fields = ('institution_name', 'title', 'price')


class DoctorSerializer(serializers.ModelSerializer):
    institution_name = serializers.ReadOnlyField(source='rel_institution.name', read_only=True)
    spec_name = serializers.ReadOnlyField(source='specialties.title', read_only=True)
    class Meta:
        model = Doctor
        fields = ['specialties', 'spec_name', 'institution_name', 'rel_institution', 'id', 'name', 'description', 'doctor_photo', 'regime', 'phone_number', 'email']


class PhoneSerializer(serializers.ModelSerializer):
    class Meta:
        model = Phone


class PhotosSerializer(serializers.ModelSerializer):
    class Meta:
        model = Photo


class ServicesSerializer(serializers.ModelSerializer):
    institution_name = serializers.ReadOnlyField(source='rel_institution.name', read_only=True)
    class Meta:
        model = Service
        fields = ('institution_name', 'title', 'full_description', 'rel_institution')



class ActionsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Action
        fields = ('id', 'photo', 'institution', 'short_description', 'description',)


class InstitutionSerializer(serializers.ModelSerializer):
    categories = CategorySerializer(many=True, read_only=True)
    doctors = DoctorSerializer(many=True, read_only=True)
    phones = PhotosSerializer(many=True, read_only=True)

    class Meta:
        model = Institution
        exclude = ('name_ru', 'name_kk', 'short_description_ru', 'short_description_kk', 'actions_ru', 'actions_kk', 'schedule_ru', 'schedule_kk')


class CommentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Comment


class SpecialtySerializer(serializers.ModelSerializer):
    doctors = DoctorSerializer(many=True, read_only=True)
    class Meta:
        model = Specialty


class PushUpSerializer(serializers.ModelSerializer):
    class Meta:
        model = PushUp
        fields = ('id', 'institution', 'token', 'favorite')


class BannerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Banner
        fields = ('id', 'institution', 'title', 'image', 'external_link', 'categories', 'start', 'end', 'deactive')