import django_filters
from medicine.models import Institution, Price, Doctor


class InstitutionFilter(django_filters.FilterSet):
    # doctor = django_filters.Filter(name='doctors', lookup_type='in')
    category = django_filters.Filter(
        name='category', lookup_type='exact'
    )
    name = django_filters.CharFilter(lookup_type='iexact')
    schedule = django_filters.CharFilter(lookup_type='icontains')

    class Meta:
        model = Institution
        fields = ('name', 'category', 'address', 'short_description', 'schedule')


class PriceFilter(django_filters.FilterSet):

    class Meta:
        model = Price
        fields = ('title', 'price')


class DoctorFilter(django_filters.FilterSet):

    class Meta:
        model = Doctor
        fields = ('name', 'specialties')