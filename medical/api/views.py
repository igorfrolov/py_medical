# -*- coding: utf-8
import datetime

from rest_framework import viewsets, filters
from rest_framework.decorators import detail_route
from rest_framework.response import Response

from medicine.models import Institution, Comment, Category, Doctor, Photo, Phone, Action, Service, Price, Specialty, PushUp, Banner
from .serializers import CommentSerializer, InstitutionSerializer, CategorySerializer, DoctorSerializer, \
    PhotosSerializer, PhoneSerializer, \
    ActionsSerializer, ServicesSerializer, PriceSerializer, SpecialtySerializer, PushUpSerializer, BannerSerializer
from .forms import InstitutionFilter


class CategoryViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer
    http_method_names = ['get', 'post']

    @detail_route(methods=['get'])
    def institutions(self, request, pk):
        cat = Category.objects.get(pk=1)
        serializer = InstitutionSerializer(Institution.objects.filter(category=cat), many=True)
        return Response(serializer.data)


class InstitutionViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Institution.objects.filter(is_active=True)
    serializer_class = InstitutionSerializer
    filter_backends = (filters.OrderingFilter, filters.DjangoFilterBackend, filters.SearchFilter)
    filter_class = InstitutionFilter
    search_fields = ('name', 'schedule', 'short_description', 'address')
    ordering_fields = ('pk',)
    ordering = ('name',)

    @detail_route(methods=['get'])
    def photos(self, request, pk):
        inst = Institution.objects.get(pk=pk)
        serializer = PhotosSerializer(Photo.objects.filter(rel_institution=inst).order_by('description'), many=True)
        return Response(serializer.data)

    @detail_route(methods=['get'])
    def services(self, request, pk):
        inst = Institution.objects.get(pk=pk)
        serializer = ServicesSerializer(Service.objects.filter(rel_institution=inst).order_by('title'), many=True)
        return Response(serializer.data)

    @detail_route(methods=['get'])
    def doctor(self, request, pk):
        inst = Institution.objects.get(pk=pk)
        serializer = DoctorSerializer(Doctor.objects.filter(rel_institution=inst).order_by('name'), many=True)
        return Response(serializer.data)

    @detail_route(methods=['get'])
    def actions(self, request, pk):
        inst = Institution.objects.get(pk=pk)
        serializer = ActionsSerializer(Action.objects.filter(institution=inst).order_by('description'), many=True)
        return Response(serializer.data)


    @detail_route(methods=['get', 'post'])
    def comments(self, request, pk):
        inst = Institution.objects.get(pk=pk)
        serializer = CommentSerializer(Comment.objects.filter(institution=inst, public=True).order_by('comment'),
                                       many=True)
        return Response(serializer.data)


class PriceViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Price.objects.all().order_by('title')
    serializer_class = PriceSerializer
    filter_backends = (filters.OrderingFilter, filters.DjangoFilterBackend, filters.SearchFilter)
    search_fields = ('title', )


class CommentsViewSet(viewsets.ModelViewSet):
    queryset = Comment.objects.filter(public=True).order_by('comment')
    serializer_class = CommentSerializer


class DoctorsViewSet(viewsets.ModelViewSet):
    queryset = Doctor.objects.all().order_by('name')
    serializer_class = DoctorSerializer
    filter_backends = (filters.OrderingFilter, filters.DjangoFilterBackend, filters.SearchFilter)
    search_fields = ('name', )


class PhotoViewSet(viewsets.ModelViewSet):
    queryset = Photo.objects.all().order_by('description')
    serializer_class = PhotosSerializer


class ServiceViewSet(viewsets.ModelViewSet):
    queryset = Service.objects.all().order_by('title')
    serializer_class = ServicesSerializer
    filter_backends = (filters.OrderingFilter, filters.DjangoFilterBackend, filters.SearchFilter)
    search_fields = ('title', 'full_description', )


class PhoneViewSet(viewsets.ModelViewSet):
    queryset = Phone.objects.all()
    serializer_class = PhoneSerializer


class ActionViewSet(viewsets.ModelViewSet):
    queryset = Action.objects.all().order_by('description')
    serializer_class = ActionsSerializer


class SpecialtyViewSet(viewsets.ModelViewSet):
    queryset = Specialty.objects.all().order_by('title')
    serializer_class = SpecialtySerializer
    filter_backends = (filters.OrderingFilter, filters.DjangoFilterBackend, filters.SearchFilter)
    search_fields = ('title',)

    @detail_route(methods=['get'])
    def doctor(self, request, pk):
        specs = Specialty.objects.get(pk=pk)
        serializer = DoctorSerializer(Doctor.objects.filter(specialties=specs).order_by('name'), many=True)
        return Response(serializer.data)


class PushUplistViewSet(viewsets.ModelViewSet):
    queryset = PushUp.objects.filter(favorite=True)
    serializer_class = PushUpSerializer


class BannerViewSet(viewsets.ModelViewSet):
    now = datetime.datetime.now().strftime("%Y-%m-%d")
    print now
    queryset = Banner.objects.filter(end__gt=now)
    print Banner.objects.filter(end=now)
    serializer_class = BannerSerializer