from django.conf.urls import include, patterns, url
from rest_framework_extensions.routers import ExtendedDefaultRouter, ExtendedSimpleRouter
from api import utils
from .views import InstitutionViewSet, CommentsViewSet, CategoryViewSet, DoctorsViewSet, PhotoViewSet, PhoneViewSet, \
    ActionViewSet, PriceViewSet, ServiceViewSet, SpecialtyViewSet, PushUplistViewSet, BannerViewSet

router = ExtendedSimpleRouter()
router.register(r'categories', CategoryViewSet, 'category')
# router.register(r'doctor', DoctorsViewSet, 'doctor')
router.register(r'doctor', DoctorsViewSet, 'doctor')
router.register(r'services', ServiceViewSet, 'service')
router.register(r'institution', InstitutionViewSet, 'institution')\
     .register(r'phones', PhoneViewSet, 'phones', parents_query_lookups=['rel_institution'])
router.register(r'comments', CommentsViewSet, 'comment')
router.register(r'institution', InstitutionViewSet, 'institution')\
       .register(r'comments', CommentsViewSet, 'comment', parents_query_lookups=['institution__pk'])
router.register(r'actions', ActionViewSet, 'action')
router.register(r'institution', InstitutionViewSet, 'institution')

router.register(r'institution', InstitutionViewSet, 'institution')
router.register(r'price', PriceViewSet, 'price')
router.register(r'comments', CommentsViewSet, 'parent-comments')
router.register(r'specialty', SpecialtyViewSet, 'specialty')
router.register(r'push', PushUplistViewSet, 'push')
router.register(r'banners', BannerViewSet, 'banner')

urlpatterns = patterns(
     '',
     url(r'^', include(router.urls)),
     url(r'^actions_count/$', utils.actions_count, name='actions_count'),
)
