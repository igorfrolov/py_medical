$(document).ready(function () {
    $('#id_farm').on('click', function (e) {
        if ($(e.currentTarget).is(':checked')) {
            document.getElementById('id_photo').style.display = 'none';
            document.getElementById('id_service').style.display = 'none';
            document.getElementById('id_doctors').style.display = 'none';
            document.getElementById('div_id_main_photo').style.display = 'none';
            document.getElementById('div_id_license_text').style.display = 'none';
            document.getElementById('div_id_license_text').style.display = 'none';
            document.getElementById('div_id_license_photo').style.display = 'none';
        }
    });


    //tinymce.init({
    //    selector: "textarea.summernote",
    //    plugins: [
    //        "advlist autolink lists link image charmap print preview anchor",
    //        "searchreplace visualblocks code fullscreen",
    //        "insertdatetime media table contextmenu paste"
    //    ],
    //    toolbar: "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
    //});

    $('#id_service textarea').each(function (index, value) {
        tinymce.init({
            mode: 'exact',
            //selector: 'textarea.summernote',
            elements: value.id,
            plugins: [
                "advlist autolink lists link image charmap print preview anchor",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media table contextmenu paste"
            ],
            toolbar: "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
        });
    });
    $('#id_actions textarea').each(function (index, value) {
        tinymce.init({
            mode: 'exact',
            //selector: 'textarea.summernote',
            elements: value.id,
            plugins: [
                "advlist autolink lists link image charmap print preview anchor",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media table contextmenu paste"
            ],
            toolbar: "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
        });
    });
    $('form').on('submit', function (e) {
        $(tinymce.get()).each(function (i, el) {
            $(el.id).val(tinyMCE.get(i).getContent());
        });
    });
});




