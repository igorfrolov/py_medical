""" Default urlconf for medical """
import settings.base as settings
from django.conf.urls import include, patterns, url
from django.contrib.auth.views import logout
from django.contrib import admin
from django.conf.urls.static import static
from django.conf.urls.i18n import i18n_patterns
admin.autodiscover()


def bad(request):
    """ Simulates a server error """
    1 / 0


urlpatterns = i18n_patterns(
    '',
    url(r'', include('base.urls', namespace='base')),
    url(r'^summernote/', include('django_summernote.urls')),
    url(r'^api/', include('api.urls')),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^bad/$', bad),
) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    urlpatterns += patterns(url(r'^media/(?P<path>.*)$', 'django.views.static.serve',
                                {'document_root': settings.MEDIA_ROOT}),)

if 'rosetta' in settings.INSTALLED_APPS:
    urlpatterns += patterns('',
        url(r'^rosetta/', include('rosetta.urls')),
    )
