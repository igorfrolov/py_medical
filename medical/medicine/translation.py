from modeltranslation.translator import translator, TranslationOptions
from .models import Category, Institution, Doctor, Service, AboutProject, Action


class CategoryTranslationOptions(TranslationOptions):
    fields = ('title',)


class InstitutionTranslationOptions(TranslationOptions):
    fields = ('name', 'short_description', 'actions', 'schedule',)


class DoctorTranslationOptions(TranslationOptions):
    fields = ('name',)


class AboutProjectTranslationOptions(TranslationOptions):
    fields = ('description',)


class ServiceTranslationOptions(TranslationOptions):
    fields = ('title', 'full_description')


class ActionTranslationOptions(TranslationOptions):
    fields = ('short_description', 'description',)


translator.register(Category, CategoryTranslationOptions)
translator.register(Institution, InstitutionTranslationOptions)
translator.register(Doctor, DoctorTranslationOptions)
translator.register(Service, ServiceTranslationOptions)
translator.register(Action, ActionTranslationOptions)
translator.register(AboutProject, AboutProjectTranslationOptions)
