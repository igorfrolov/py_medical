# -*- coding: utf-8 -*-
from django.contrib import admin
from modeltranslation.admin import TranslationAdmin
from .models import Institution, Category, Comment, Doctor, Service, Photo, AboutProject, Contact, Feedback, Action, Specialty


class AboutProjectAdmin(TranslationAdmin):
    list_display = ('description',)

admin.site.register(AboutProject, AboutProjectAdmin)


class CategoryAdmin(TranslationAdmin):
    list_display = ['title']
admin.site.register(Category, CategoryAdmin)


class InstitutionInline(admin.StackedInline):
    model = Doctor
    extra = 1


class InstitutionPhotoInline(admin.StackedInline):
    model = Photo
    extra = 1


class InstitutionActionInline(admin.StackedInline):
    model = Action
    extra = 1


class InstitutionAdmin(TranslationAdmin):
    inlines = [InstitutionInline, InstitutionPhotoInline, InstitutionActionInline]

admin.site.register(Institution, InstitutionAdmin)


class DoctorAdmin(TranslationAdmin):
    list_display = ['name']
admin.site.register(Doctor, DoctorAdmin)


class ServiceAdmin(TranslationAdmin):
    list_display = ['title', 'full_description', 'price']
admin.site.register(Service, ServiceAdmin)

admin.site.register(Comment)

admin.site.register(Contact)

admin.site.register(Feedback)


class ActionAdmin(TranslationAdmin):
    list_display = ['short_description', 'description']
admin.site.register(Action, ActionAdmin)

admin.site.register(Specialty)