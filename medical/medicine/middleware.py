from django.middleware.locale import LocaleMiddleware
from django.conf import settings
from django.utils import translation


class SwitchLocale(LocaleMiddleware):
    def process_request(self, request):
        if 'lang' in request.GET:
            language = request.GET['lang']
        else:
            language = settings.LANGUAGE_CODE

        translation.activate(language)
        request.LANGUAGE_CODE = language

