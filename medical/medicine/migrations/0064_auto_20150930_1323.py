# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import medicine.models


class Migration(migrations.Migration):

    dependencies = [
        ('medicine', '0063_doctor_specialties'),
    ]

    operations = [
        migrations.AlterField(
            model_name='action',
            name='photo',
            field=models.ImageField(upload_to=medicine.models.get_action_photos_path, null=True, verbose_name='\u0424\u043e\u0442\u043e \u0434\u043b\u044f \u0430\u043a\u0446\u0438\u0438', blank=True),
        ),
        migrations.AlterField(
            model_name='banner',
            name='image',
            field=models.ImageField(upload_to=medicine.models.get_banner_photos_path, verbose_name='\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435'),
        ),
        migrations.AlterField(
            model_name='category',
            name='background',
            field=models.ImageField(upload_to=medicine.models.get_bg_path, null=True, verbose_name='\u0424\u043e\u043d\u043e\u0432\u043e\u0435 \u0438\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435', blank=True),
        ),
        migrations.AlterField(
            model_name='category',
            name='logo',
            field=models.ImageField(upload_to=medicine.models.get_image_path, null=True, verbose_name='\u041b\u043e\u0433\u043e\u0442\u0438\u043f', blank=True),
        ),
        migrations.AlterField(
            model_name='doctor',
            name='doctor_photo',
            field=models.ImageField(upload_to=medicine.models.get_doctor_photo_path, null=True, verbose_name='\u0424\u043e\u0442\u043e', blank=True),
        ),
        migrations.AlterField(
            model_name='institution',
            name='license_photo',
            field=models.ImageField(upload_to=medicine.models.get_license_photo_path, null=True, verbose_name='\u0424\u043e\u0442\u043e \u043b\u0438\u0446\u0435\u043d\u0437\u0438\u0438', blank=True),
        ),
        migrations.AlterField(
            model_name='institution',
            name='logo',
            field=models.ImageField(upload_to=medicine.models.get_logos_path, null=True, verbose_name='\u041b\u043e\u0433\u043e\u0442\u0438\u043f', blank=True),
        ),
        migrations.AlterField(
            model_name='institution',
            name='main_photo',
            field=models.ImageField(upload_to=medicine.models.get_main_photo_path, null=True, verbose_name='\u0424\u043e\u0442\u043e', blank=True),
        ),
        migrations.AlterField(
            model_name='photo',
            name='photo',
            field=models.ImageField(upload_to=medicine.models.get_photo_path, null=True, verbose_name='\u0424\u043e\u0442\u043e', blank=True),
        ),
        migrations.AlterField(
            model_name='specialty',
            name='logo',
            field=models.FileField(upload_to=medicine.models.get_logo_spec_path, null=True, verbose_name='\u041b\u043e\u0433\u043e\u0442\u0438\u043f', blank=True),
        ),
    ]
