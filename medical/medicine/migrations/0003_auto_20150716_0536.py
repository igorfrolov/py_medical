# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('medicine', '0002_auto_20150716_0459'),
    ]

    operations = [
        migrations.AlterField(
            model_name='institution',
            name='category',
            field=models.ManyToManyField(related_name='categories', verbose_name=b'\xd0\x9a\xd0\xb0\xd1\x82\xd0\xb5\xd0\xb3\xd0\xbe\xd1\x80\xd0\xb8\xd1\x8f', to='medicine.Category'),
        ),
    ]
