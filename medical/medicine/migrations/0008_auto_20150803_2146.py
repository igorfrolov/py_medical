# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django_resized.forms
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('medicine', '0007_aboutproject_contact_feedback'),
    ]

    operations = [
        migrations.AlterField(
            model_name='aboutproject',
            name='description',
            field=models.TextField(verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435 \u043f\u0440\u043e\u0435\u043a\u0442\u0430'),
        ),
        migrations.AlterField(
            model_name='category',
            name='background',
            field=models.ImageField(upload_to=b'category_backgrounds', verbose_name='\u0424\u043e\u043d\u043e\u0432\u043e\u0435 \u0438\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435', blank=True),
        ),
        migrations.AlterField(
            model_name='category',
            name='logo',
            field=models.ImageField(upload_to=b'category_logos', verbose_name='\u041b\u043e\u0433\u043e\u0442\u0438\u043f'),
        ),
        migrations.AlterField(
            model_name='category',
            name='title',
            field=models.CharField(max_length=255, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435'),
        ),
        migrations.AlterField(
            model_name='comment',
            name='comment',
            field=models.TextField(verbose_name='\u041a\u043e\u043c\u0435\u043d\u0442\u0430\u0440\u0438\u0439'),
        ),
        migrations.AlterField(
            model_name='comment',
            name='commentator_name',
            field=models.CharField(max_length=255, verbose_name='\u0418\u043c\u044f'),
        ),
        migrations.AlterField(
            model_name='comment',
            name='institution',
            field=models.ForeignKey(verbose_name='\u0417\u0430\u0432\u0435\u0434\u0435\u043d\u0438\u0435', to='medicine.Institution'),
        ),
        migrations.AlterField(
            model_name='contact',
            name='address',
            field=models.CharField(max_length=2048, verbose_name='\u0410\u0434\u0440\u0435\u0441'),
        ),
        migrations.AlterField(
            model_name='contact',
            name='email',
            field=models.EmailField(max_length=128, verbose_name='\u041f\u043e\u0447\u0442\u0430'),
        ),
        migrations.AlterField(
            model_name='contact',
            name='phone',
            field=models.CharField(max_length=13, verbose_name='\u0422\u0435\u043b\u0435\u0444\u043e\u043d'),
        ),
        migrations.AlterField(
            model_name='doctor',
            name='doctor_photo',
            field=django_resized.forms.ResizedImageField(upload_to=b'doctors_photos', verbose_name='\u0424\u043e\u0442\u043e', blank=True),
        ),
        migrations.AlterField(
            model_name='doctor',
            name='doctor_to_home',
            field=models.BooleanField(verbose_name='\u0412\u043e\u0437\u043c\u043e\u0436\u043d\u043e\u0441\u0442\u044c \u0432\u044b\u0437\u043e\u0432\u0430 \u043d\u0430 \u0434\u043e\u043c'),
        ),
        migrations.AlterField(
            model_name='doctor',
            name='experience',
            field=models.CharField(max_length=255, verbose_name='\u0421\u0442\u0430\u0436 \u0440\u0430\u0431\u043e\u0442\u044b'),
        ),
        migrations.AlterField(
            model_name='doctor',
            name='name',
            field=models.CharField(max_length=255, verbose_name='\u0424\u0418\u041e'),
        ),
        migrations.AlterField(
            model_name='doctor',
            name='regime',
            field=models.CharField(default=b'', max_length=255, verbose_name='\u0420\u0435\u0436\u0438\u043c \u0440\u0430\u0431\u043e\u0442\u044b'),
        ),
        migrations.AlterField(
            model_name='doctor',
            name='specs',
            field=models.CharField(max_length=1000, verbose_name='\u0421\u043f\u0435\u0446\u0438\u0430\u043b\u0438\u0437\u0430\u0446\u0438\u044f'),
        ),
        migrations.AlterField(
            model_name='feedback',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, verbose_name='\u0414\u0430\u0442\u0430 \u0441\u043e\u0437\u0434\u0430\u043d\u0438\u044f'),
        ),
        migrations.AlterField(
            model_name='feedback',
            name='email',
            field=models.CharField(max_length=128, null=True, verbose_name='\u041f\u043e\u0447\u0442\u0430'),
        ),
        migrations.AlterField(
            model_name='feedback',
            name='message',
            field=models.TextField(verbose_name='\u0421\u043e\u043e\u0431\u0449\u0435\u043d\u0438\u0435'),
        ),
        migrations.AlterField(
            model_name='feedback',
            name='name',
            field=models.CharField(max_length=256, verbose_name='\u0418\u043c\u044f'),
        ),
        migrations.AlterField(
            model_name='feedback',
            name='phone',
            field=models.CharField(max_length=13, verbose_name='\u0422\u0435\u043b\u0435\u0444\u043e\u043d'),
        ),
        migrations.AlterField(
            model_name='institution',
            name='actions',
            field=models.CharField(max_length=255, verbose_name='\u0410\u043a\u0446\u0438\u0438', blank=True),
        ),
        migrations.AlterField(
            model_name='institution',
            name='address',
            field=models.CharField(max_length=255, verbose_name='\u0410\u0434\u0440\u0435\u0441'),
        ),
        migrations.AlterField(
            model_name='institution',
            name='category',
            field=models.ManyToManyField(related_name='categories', verbose_name='\u041a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u044f', to='medicine.Category'),
        ),
        migrations.AlterField(
            model_name='institution',
            name='email',
            field=models.EmailField(max_length=255, verbose_name='\u041f\u043e\u0447\u0442\u0430'),
        ),
        migrations.AlterField(
            model_name='institution',
            name='license_photo',
            field=models.ImageField(upload_to=b'', verbose_name='\u0424\u043e\u0442\u043e \u043b\u0438\u0446\u0435\u043d\u0437\u0438\u0438', blank=True),
        ),
        migrations.AlterField(
            model_name='institution',
            name='license_text',
            field=models.CharField(max_length=255, verbose_name='\u041b\u0438\u0446\u0435\u043d\u0437\u0438\u044f'),
        ),
        migrations.AlterField(
            model_name='institution',
            name='logo',
            field=django_resized.forms.ResizedImageField(upload_to=b'logos', verbose_name='\u041b\u043e\u0433\u043e\u0442\u0438\u043f'),
        ),
        migrations.AlterField(
            model_name='institution',
            name='name',
            field=models.CharField(max_length=255, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435'),
        ),
        migrations.AlterField(
            model_name='institution',
            name='price_list_desc',
            field=models.TextField(default=b'', max_length=1000, verbose_name='\u041f\u0440\u0430\u0439\u0441 \u041b\u0438\u0441\u0442', blank=True),
        ),
        migrations.AlterField(
            model_name='institution',
            name='price_list_file',
            field=models.FileField(default=None, upload_to=b'', verbose_name='\u0424\u0430\u0439\u043b', blank=True),
        ),
        migrations.AlterField(
            model_name='institution',
            name='schedule',
            field=models.CharField(max_length=255, verbose_name='\u0420\u0430\u0441\u043f\u0438\u0441\u0430\u043d\u0438\u0435'),
        ),
        migrations.AlterField(
            model_name='institution',
            name='services',
            field=models.TextField(verbose_name='\u0423\u0441\u043b\u0443\u0433\u0438'),
        ),
        migrations.AlterField(
            model_name='institution',
            name='short_description',
            field=models.TextField(verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435'),
        ),
        migrations.AlterField(
            model_name='institution',
            name='website',
            field=models.CharField(max_length=255, verbose_name='\u0421\u0430\u0439\u0442', blank=True),
        ),
        migrations.AlterField(
            model_name='phone',
            name='phone_number',
            field=models.CharField(max_length=13, verbose_name='\u0422\u0435\u043b\u0435\u0444\u043e\u043d (+996XXXXXXXXX)', validators=[django.core.validators.RegexValidator(regex=b'^\\+?1?\\d{9,15}$')]),
        ),
        migrations.AlterField(
            model_name='photo',
            name='photo',
            field=django_resized.forms.ResizedImageField(upload_to=b'institution_photos', verbose_name='\u0424\u043e\u0442\u043e'),
        ),
    ]
