# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('medicine', '0056_auto_20150923_2249'),
    ]

    operations = [
        migrations.AddField(
            model_name='banner',
            name='deactive',
            field=models.BooleanField(default=False),
        ),
    ]
