# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('medicine', '0012_auto_20150811_1721'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='institution',
            name='price_list_desc',
        ),
        migrations.RemoveField(
            model_name='institution',
            name='price_list_file',
        ),
        migrations.RemoveField(
            model_name='institution',
            name='services',
        ),
        migrations.AddField(
            model_name='service',
            name='institution',
            field=models.ForeignKey(default=11, to='medicine.Institution'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='service',
            name='price',
            field=models.CharField(default=11, max_length=255, verbose_name='\u0426\u0435\u043d\u0430 \u0443\u0441\u043b\u0443\u0433\u0438'),
            preserve_default=False,
        ),
    ]
