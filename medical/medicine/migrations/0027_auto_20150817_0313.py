# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('medicine', '0026_auto_20150817_0300'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='aboutproject',
            name='description_kk',
        ),
        migrations.RemoveField(
            model_name='aboutproject',
            name='description_ru',
        ),
        migrations.AddField(
            model_name='service',
            name='price_kk',
            field=models.CharField(max_length=255, null=True, verbose_name='\u0426\u0435\u043d\u0430 \u0443\u0441\u043b\u0443\u0433\u0438', blank=True),
        ),
        migrations.AddField(
            model_name='service',
            name='price_ru',
            field=models.CharField(max_length=255, null=True, verbose_name='\u0426\u0435\u043d\u0430 \u0443\u0441\u043b\u0443\u0433\u0438', blank=True),
        ),
        migrations.AddField(
            model_name='service',
            name='title_kk',
            field=models.CharField(max_length=255, null=True, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435'),
        ),
        migrations.AddField(
            model_name='service',
            name='title_ru',
            field=models.CharField(max_length=255, null=True, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435'),
        ),
    ]
