# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('medicine', '0002_auto_20150716_0459'),
    ]

    operations = [
        migrations.CreateModel(
            name='Phone',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('phone_number', models.CharField(max_length=13, verbose_name=b'\xd0\xa2\xd0\xb5\xd0\xbb\xd0\xb5\xd1\x84\xd0\xbe\xd0\xbd (+996XXXXXXXXX)', validators=[django.core.validators.RegexValidator(regex=b'^\\+?1?\\d{9,15}$')])),
            ],
        ),
        migrations.RemoveField(
            model_name='institution',
            name='phone',
        ),
        migrations.AlterField(
            model_name='institution',
            name='email',
            field=models.EmailField(max_length=255, verbose_name=b'\xd0\x9f\xd0\xbe\xd1\x87\xd1\x82\xd0\xb0'),
        ),
        migrations.AddField(
            model_name='phone',
            name='rel_institution',
            field=models.ForeignKey(to='medicine.Institution'),
        ),
    ]
