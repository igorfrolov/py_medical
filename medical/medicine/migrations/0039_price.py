# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('medicine', '0038_auto_20150818_2250'),
    ]

    operations = [
        migrations.CreateModel(
            name='Price',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=500, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
                ('price', models.CharField(max_length=500, verbose_name='\u0426\u0435\u043d\u0430')),
                ('institution', models.ForeignKey(to='medicine.Institution')),
            ],
            options={
                'verbose_name': '\u041f\u0440\u0430\u0439\u0441',
                'verbose_name_plural': '\u041f\u0440\u0430\u0439\u0441\u044b',
            },
        ),
    ]
