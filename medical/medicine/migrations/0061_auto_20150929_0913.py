# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('medicine', '0060_doctor_description'),
    ]

    operations = [
        migrations.AlterField(
            model_name='category',
            name='title',
            field=models.CharField(max_length=255, null=True, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435', blank=True),
        ),
        migrations.AlterField(
            model_name='category',
            name='title_kk',
            field=models.CharField(max_length=255, null=True, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435', blank=True),
        ),
        migrations.AlterField(
            model_name='category',
            name='title_ru',
            field=models.CharField(max_length=255, null=True, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435', blank=True),
        ),
        migrations.AlterField(
            model_name='phone',
            name='phone_number',
            field=models.CharField(blank=True, max_length=13, null=True, verbose_name='\u0422\u0435\u043b\u0435\u0444\u043e\u043d (+996XXXXXXXXX)', validators=[django.core.validators.RegexValidator(regex=b'^\\+?1?\\d{9,15}$')]),
        ),
        migrations.AlterField(
            model_name='specialty',
            name='logo',
            field=models.FileField(upload_to=b'Specialty/logo', null=True, verbose_name='\u041b\u043e\u0433\u043e\u0442\u0438\u043f', blank=True),
        ),
        migrations.AlterField(
            model_name='specialty',
            name='title',
            field=models.CharField(max_length=256, null=True, verbose_name='\u0421\u043f\u0435\u0446\u0438\u0430\u043b\u044c\u043d\u043e\u0441\u0442\u044c', blank=True),
        ),
    ]
