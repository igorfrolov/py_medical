# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django_resized.forms


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name=b'\xd0\x9d\xd0\xb0\xd0\xb7\xd0\xb2\xd0\xb0\xd0\xbd\xd0\xb8\xd0\xb5')),
                ('logo', models.ImageField(upload_to=b'category_logos', verbose_name=b'\xd0\x9b\xd0\xbe\xd0\xb3\xd0\xbe\xd1\x82\xd0\xb8\xd0\xbf')),
                ('background', models.ImageField(upload_to=b'category_backgrounds', verbose_name=b'\xd0\xa4\xd0\xbe\xd0\xbd\xd0\xbe\xd0\xb2\xd0\xbe\xd0\xb5 \xd0\xb8\xd0\xb7\xd0\xbe\xd0\xb1\xd1\x80\xd0\xb0\xd0\xb6\xd0\xb5\xd0\xbd\xd0\xb8\xd0\xb5', blank=True)),
            ],
            options={
                'verbose_name': '\u041a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u044f',
                'verbose_name_plural': '\u041a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u0438',
            },
        ),
        migrations.CreateModel(
            name='Comment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('commentator_name', models.CharField(max_length=255, verbose_name=b'\xd0\x98\xd0\xbc\xd1\x8f')),
                ('comment', models.TextField(verbose_name=b'\xd0\x9a\xd0\xbe\xd0\xbc\xd0\xb5\xd0\xbd\xd1\x82\xd0\xb0\xd1\x80\xd0\xb8\xd0\xb9')),
            ],
            options={
                'verbose_name': '\u041e\u0442\u0437\u044b\u0432',
                'verbose_name_plural': '\u041e\u0442\u0437\u044b\u0432\u044b',
            },
        ),
        migrations.CreateModel(
            name='Doctor',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name=b'\xd0\xa4\xd0\x98\xd0\x9e')),
                ('doctor_photo', django_resized.forms.ResizedImageField(upload_to=b'doctors_photos', verbose_name=b'\xd0\xa4\xd0\xbe\xd1\x82\xd0\xbe', blank=True)),
                ('specs', models.CharField(max_length=1000, verbose_name=b'\xd0\xa1\xd0\xbf\xd0\xb5\xd1\x86\xd0\xb8\xd0\xb0\xd0\xbb\xd0\xb8\xd0\xb7\xd0\xb0\xd1\x86\xd0\xb8\xd1\x8f')),
                ('experience', models.CharField(max_length=255, verbose_name=b'\xd0\xa1\xd1\x82\xd0\xb0\xd0\xb6 \xd1\x80\xd0\xb0\xd0\xb1\xd0\xbe\xd1\x82\xd1\x8b')),
                ('regime', models.CharField(default=b'', max_length=255, verbose_name=b'\xd0\xa0\xd0\xb5\xd0\xb6\xd0\xb8\xd0\xbc \xd1\x80\xd0\xb0\xd0\xb1\xd0\xbe\xd1\x82\xd1\x8b')),
                ('doctor_to_home', models.BooleanField(verbose_name=b'\xd0\x92\xd0\xbe\xd0\xb7\xd0\xbc\xd0\xbe\xd0\xb6\xd0\xbd\xd0\xbe\xd1\x81\xd1\x82\xd1\x8c \xd0\xb2\xd1\x8b\xd0\xb7\xd0\xbe\xd0\xb2\xd0\xb0 \xd0\xbd\xd0\xb0 \xd0\xb4\xd0\xbe\xd0\xbc')),
            ],
            options={
                'verbose_name': '\u0412\u0440\u0430\u0447',
                'verbose_name_plural': '\u0412\u0440\u0430\u0447\u0438',
            },
        ),
        migrations.CreateModel(
            name='Institution',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('logo', django_resized.forms.ResizedImageField(upload_to=b'logos', verbose_name=b'\xd0\x9b\xd0\xbe\xd0\xb3\xd0\xbe\xd1\x82\xd0\xb8\xd0\xbf')),
                ('name', models.CharField(max_length=255, verbose_name=b'\xd0\x9d\xd0\xb0\xd0\xb7\xd0\xb2\xd0\xb0\xd0\xbd\xd0\xb8\xd0\xb5')),
                ('short_description', models.TextField(verbose_name=b'\xd0\x9e\xd0\xbf\xd0\xb8\xd1\x81\xd0\xb0\xd0\xbd\xd0\xb8\xd0\xb5')),
                ('services', models.TextField(verbose_name=b'\xd0\xa3\xd1\x81\xd0\xbb\xd1\x83\xd0\xb3\xd0\xb8')),
                ('license_text', models.CharField(max_length=255, verbose_name=b'\xd0\x9b\xd0\xb8\xd1\x86\xd0\xb5\xd0\xbd\xd0\xb7\xd0\xb8\xd1\x8f')),
                ('license_photo', models.ImageField(upload_to=b'', verbose_name=b'\xd0\xa4\xd0\xbe\xd1\x82\xd0\xbe \xd0\xbb\xd0\xb8\xd1\x86\xd0\xb5\xd0\xbd\xd0\xb7\xd0\xb8\xd0\xb8', blank=True)),
                ('actions', models.CharField(max_length=255, verbose_name=b'\xd0\x90\xd0\xba\xd1\x86\xd0\xb8\xd0\xb8', blank=True)),
                ('schedule', models.CharField(max_length=255, verbose_name=b'\xd0\xa0\xd0\xb0\xd1\x81\xd0\xbf\xd0\xb8\xd1\x81\xd0\xb0\xd0\xbd\xd0\xb8\xd0\xb5')),
                ('phone', models.CharField(max_length=255, verbose_name=b'\xd0\xa2\xd0\xb5\xd0\xbb\xd0\xb5\xd1\x84\xd0\xbe\xd0\xbd')),
                ('email', models.CharField(max_length=255, verbose_name=b'\xd0\x9f\xd0\xbe\xd1\x87\xd1\x82\xd0\xb0')),
                ('address', models.CharField(max_length=255, verbose_name=b'\xd0\x90\xd0\xb4\xd1\x80\xd0\xb5\xd1\x81')),
                ('longitude', models.DecimalField(default=b'', verbose_name='\u0428\u0438\u0440\u0438\u043d\u0430', max_digits=13, decimal_places=6)),
                ('latitude', models.DecimalField(default=b'', verbose_name='\u0414\u043e\u043b\u0433\u043e\u0442\u0430', max_digits=13, decimal_places=6)),
                ('website', models.CharField(max_length=255, verbose_name=b'\xd0\xa1\xd0\xb0\xd0\xb9\xd1\x82', blank=True)),
                ('price_list_desc', models.CharField(default=b'', max_length=1000, verbose_name=b'\xd0\x9f\xd1\x80\xd0\xb0\xd0\xb9\xd1\x81 \xd0\x9b\xd0\xb8\xd1\x81\xd1\x82', blank=True)),
                ('price_list_file', models.FileField(default=None, upload_to=b'', verbose_name=b'\xd0\xa4\xd0\xb0\xd0\xb9\xd0\xbb')),
                ('category', models.ForeignKey(verbose_name=b'\xd0\x9a\xd0\xb0\xd1\x82\xd0\xb5\xd0\xb3\xd0\xbe\xd1\x80\xd0\xb8\xd1\x8f', to='medicine.Category')),
            ],
            options={
                'verbose_name': '\u0417\u0430\u0432\u0435\u0434\u0435\u043d\u0438\u0435',
                'verbose_name_plural': '\u0417\u0430\u0432\u0435\u0434\u0435\u043d\u0438\u044f',
            },
        ),
        migrations.CreateModel(
            name='Photo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('photo', django_resized.forms.ResizedImageField(upload_to=b'institution_photos', verbose_name=b'\xd0\xa4\xd0\xbe\xd1\x82\xd0\xbe')),
                ('rel_institution', models.ForeignKey(to='medicine.Institution')),
            ],
            options={
                'verbose_name': '\u0424\u043e\u0442\u043e',
                'verbose_name_plural': '\u0424\u043e\u0442\u043e',
            },
        ),
        migrations.AddField(
            model_name='doctor',
            name='rel_institution',
            field=models.ForeignKey(to='medicine.Institution'),
        ),
        migrations.AddField(
            model_name='comment',
            name='institution',
            field=models.ForeignKey(verbose_name=b'\xd0\x97\xd0\xb0\xd0\xb2\xd0\xb5\xd0\xb4\xd0\xb5\xd0\xbd\xd0\xb8\xd0\xb5', to='medicine.Institution'),
        ),
    ]
