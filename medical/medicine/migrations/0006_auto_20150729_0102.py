# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('medicine', '0005_auto_20150717_0959'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='phone',
            options={'verbose_name': '\u0422\u0435\u043b\u0435\u0444\u043e\u043d', 'verbose_name_plural': '\u0422\u0435\u043b\u0435\u0444\u043e\u043d\u044b'},
        ),
    ]
