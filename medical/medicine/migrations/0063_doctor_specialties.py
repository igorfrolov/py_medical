# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('medicine', '0062_remove_doctor_speciality'),
    ]

    operations = [
        migrations.AddField(
            model_name='doctor',
            name='specialties',
            field=models.ManyToManyField(to='medicine.Specialty', null=True, verbose_name='\u0421\u043f\u0435\u0446\u0438\u0430\u043b\u044c\u043d\u043e\u0441\u0442\u044c', blank=True),
        ),
    ]
