# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django_resized.forms


class Migration(migrations.Migration):

    dependencies = [
        ('medicine', '0010_doctor_phone_number'),
    ]

    operations = [
        migrations.CreateModel(
            name='Action',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('description', models.TextField(verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435 \u0430\u043a\u0446\u0438\u0438')),
                ('photo', django_resized.forms.ResizedImageField(upload_to=b'action_photos', verbose_name='\u0424\u043e\u0442\u043e \u0434\u043b\u044f \u0430\u043a\u0446\u0438\u0438')),
                ('institution', models.ForeignKey(to='medicine.Institution')),
            ],
            options={
                'verbose_name': '\u0410\u043a\u0446\u0438\u044f',
                'verbose_name_plural': '\u0410\u043a\u0446\u0438\u0438',
            },
        ),
    ]
