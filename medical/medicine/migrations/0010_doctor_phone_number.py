# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('medicine', '0009_photo_description'),
    ]

    operations = [
        migrations.AddField(
            model_name='doctor',
            name='phone_number',
            field=models.CharField(max_length=13, null=True, verbose_name='\u0422\u0435\u043b\u0435\u0444\u043e\u043d (+996XXXXXXXXX)'),
        ),
    ]
