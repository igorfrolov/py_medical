# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('medicine', '0052_remove_doctor_specs'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='banner',
            name='url',
        ),
    ]
