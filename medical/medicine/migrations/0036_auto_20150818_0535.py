# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django_resized.forms


class Migration(migrations.Migration):

    dependencies = [
        ('medicine', '0035_auto_20150818_0220'),
    ]

    operations = [
        migrations.AlterField(
            model_name='institution',
            name='actions',
            field=models.CharField(max_length=255, null=True, verbose_name='\u0410\u043a\u0446\u0438\u0438', blank=True),
        ),
        migrations.AlterField(
            model_name='institution',
            name='address',
            field=models.CharField(max_length=255, null=True, verbose_name='\u0410\u0434\u0440\u0435\u0441', blank=True),
        ),
        migrations.AlterField(
            model_name='institution',
            name='category',
            field=models.ManyToManyField(related_name='categories', null=True, verbose_name='\u041a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u044f', to='medicine.Category', blank=True),
        ),
        migrations.AlterField(
            model_name='institution',
            name='email',
            field=models.EmailField(max_length=255, null=True, verbose_name='\u041f\u043e\u0447\u0442\u0430', blank=True),
        ),
        migrations.AlterField(
            model_name='institution',
            name='latitude',
            field=models.FloatField(default=0, null=True, verbose_name='\u0428\u0438\u0440\u043e\u0442\u0430', blank=True),
        ),
        migrations.AlterField(
            model_name='institution',
            name='license_text',
            field=models.CharField(max_length=255, null=True, verbose_name='\u041b\u0438\u0446\u0435\u043d\u0437\u0438\u044f', blank=True),
        ),
        migrations.AlterField(
            model_name='institution',
            name='logo',
            field=django_resized.forms.ResizedImageField(upload_to=b'logos', null=True, verbose_name='\u041b\u043e\u0433\u043e\u0442\u0438\u043f', blank=True),
        ),
        migrations.AlterField(
            model_name='institution',
            name='longitude',
            field=models.FloatField(default=0, null=True, verbose_name='\u0414\u043e\u043b\u0433\u043e\u0442\u0430', blank=True),
        ),
        migrations.AlterField(
            model_name='institution',
            name='name',
            field=models.CharField(max_length=255, null=True, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435', blank=True),
        ),
        migrations.AlterField(
            model_name='institution',
            name='name_kk',
            field=models.CharField(max_length=255, null=True, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435', blank=True),
        ),
        migrations.AlterField(
            model_name='institution',
            name='name_ru',
            field=models.CharField(max_length=255, null=True, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435', blank=True),
        ),
        migrations.AlterField(
            model_name='institution',
            name='schedule',
            field=models.CharField(max_length=255, null=True, verbose_name='\u0420\u0430\u0441\u043f\u0438\u0441\u0430\u043d\u0438\u0435', blank=True),
        ),
        migrations.AlterField(
            model_name='institution',
            name='schedule_kk',
            field=models.CharField(max_length=255, null=True, verbose_name='\u0420\u0430\u0441\u043f\u0438\u0441\u0430\u043d\u0438\u0435', blank=True),
        ),
        migrations.AlterField(
            model_name='institution',
            name='schedule_ru',
            field=models.CharField(max_length=255, null=True, verbose_name='\u0420\u0430\u0441\u043f\u0438\u0441\u0430\u043d\u0438\u0435', blank=True),
        ),
        migrations.AlterField(
            model_name='institution',
            name='telephone',
            field=models.CharField(max_length=500, null=True, verbose_name='\u0422\u0435\u043b\u0435\u0444\u043e\u043d\u044b', blank=True),
        ),
        migrations.AlterField(
            model_name='institution',
            name='website',
            field=models.CharField(max_length=255, null=True, verbose_name='\u0421\u0430\u0439\u0442', blank=True),
        ),
    ]
