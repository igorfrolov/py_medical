# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('medicine', '0025_auto_20150817_0246'),
    ]

    operations = [
        migrations.AddField(
            model_name='doctor',
            name='name_kk',
            field=models.CharField(max_length=255, null=True, verbose_name='\u0424\u0418\u041e'),
        ),
        migrations.AddField(
            model_name='doctor',
            name='name_ru',
            field=models.CharField(max_length=255, null=True, verbose_name='\u0424\u0418\u041e'),
        ),
        migrations.AddField(
            model_name='doctor',
            name='specs_kk',
            field=models.CharField(max_length=1000, null=True, verbose_name='\u0421\u043f\u0435\u0446\u0438\u0430\u043b\u0438\u0437\u0430\u0446\u0438\u044f'),
        ),
        migrations.AddField(
            model_name='doctor',
            name='specs_ru',
            field=models.CharField(max_length=1000, null=True, verbose_name='\u0421\u043f\u0435\u0446\u0438\u0430\u043b\u0438\u0437\u0430\u0446\u0438\u044f'),
        ),
    ]
