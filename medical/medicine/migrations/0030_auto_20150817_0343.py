# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('medicine', '0029_auto_20150817_0319'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='institution',
            name='license_text_kk',
        ),
        migrations.RemoveField(
            model_name='institution',
            name='license_text_ru',
        ),
    ]
