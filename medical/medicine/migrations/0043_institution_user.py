# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('medicine', '0042_institution_is_active'),
    ]

    operations = [
        migrations.AddField(
            model_name='institution',
            name='user',
            field=models.ForeignKey(verbose_name='\u042e\u0437\u0435\u0440', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
    ]
