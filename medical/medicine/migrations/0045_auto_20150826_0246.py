# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('medicine', '0044_auto_20150825_0033'),
    ]

    operations = [
        migrations.AlterField(
            model_name='action',
            name='description',
            field=models.TextField(null=True, verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435 \u0430\u043a\u0446\u0438\u0438', blank=True),
        ),
        migrations.AlterField(
            model_name='action',
            name='description_kk',
            field=models.TextField(null=True, verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435 \u0430\u043a\u0446\u0438\u0438', blank=True),
        ),
        migrations.AlterField(
            model_name='action',
            name='description_ru',
            field=models.TextField(null=True, verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435 \u0430\u043a\u0446\u0438\u0438', blank=True),
        ),
        migrations.AlterField(
            model_name='action',
            name='photo',
            field=models.ImageField(upload_to=b'action_photos', null=True, verbose_name='\u0424\u043e\u0442\u043e \u0434\u043b\u044f \u0430\u043a\u0446\u0438\u0438', blank=True),
        ),
        migrations.AlterField(
            model_name='contact',
            name='address',
            field=models.CharField(max_length=2048, null=True, verbose_name='\u0410\u0434\u0440\u0435\u0441', blank=True),
        ),
        migrations.AlterField(
            model_name='contact',
            name='email',
            field=models.EmailField(max_length=128, null=True, verbose_name='\u041f\u043e\u0447\u0442\u0430', blank=True),
        ),
        migrations.AlterField(
            model_name='contact',
            name='phone',
            field=models.CharField(max_length=13, null=True, verbose_name='\u0422\u0435\u043b\u0435\u0444\u043e\u043d', blank=True),
        ),
        migrations.AlterField(
            model_name='doctor',
            name='doctor_photo',
            field=models.ImageField(upload_to=b'doctors_photos', null=True, verbose_name='\u0424\u043e\u0442\u043e', blank=True),
        ),
        migrations.AlterField(
            model_name='doctor',
            name='doctor_to_home',
            field=models.BooleanField(default=False, verbose_name='\u0412\u043e\u0437\u043c\u043e\u0436\u043d\u043e\u0441\u0442\u044c \u0432\u044b\u0437\u043e\u0432\u0430 \u043d\u0430 \u0434\u043e\u043c'),
        ),
        migrations.AlterField(
            model_name='doctor',
            name='email',
            field=models.EmailField(max_length=254, null=True, verbose_name='Email', blank=True),
        ),
        migrations.AlterField(
            model_name='doctor',
            name='experience',
            field=models.CharField(max_length=255, null=True, verbose_name='\u0421\u0442\u0430\u0436 \u0440\u0430\u0431\u043e\u0442\u044b', blank=True),
        ),
        migrations.AlterField(
            model_name='doctor',
            name='name',
            field=models.CharField(max_length=255, null=True, verbose_name='\u0424\u0418\u041e', blank=True),
        ),
        migrations.AlterField(
            model_name='doctor',
            name='name_kk',
            field=models.CharField(max_length=255, null=True, verbose_name='\u0424\u0418\u041e', blank=True),
        ),
        migrations.AlterField(
            model_name='doctor',
            name='name_ru',
            field=models.CharField(max_length=255, null=True, verbose_name='\u0424\u0418\u041e', blank=True),
        ),
        migrations.AlterField(
            model_name='doctor',
            name='phone_number',
            field=models.CharField(max_length=13, null=True, verbose_name='\u0422\u0435\u043b\u0435\u0444\u043e\u043d (+996XXXXXXXXX)', blank=True),
        ),
        migrations.AlterField(
            model_name='doctor',
            name='regime',
            field=models.CharField(default=b'', max_length=255, null=True, verbose_name='\u0420\u0435\u0436\u0438\u043c \u0440\u0430\u0431\u043e\u0442\u044b', blank=True),
        ),
        migrations.AlterField(
            model_name='doctor',
            name='specs',
            field=models.CharField(max_length=1000, null=True, verbose_name='\u0421\u043f\u0435\u0446\u0438\u0430\u043b\u0438\u0437\u0430\u0446\u0438\u044f', blank=True),
        ),
        migrations.AlterField(
            model_name='doctor',
            name='specs_kk',
            field=models.CharField(max_length=1000, null=True, verbose_name='\u0421\u043f\u0435\u0446\u0438\u0430\u043b\u0438\u0437\u0430\u0446\u0438\u044f', blank=True),
        ),
        migrations.AlterField(
            model_name='doctor',
            name='specs_ru',
            field=models.CharField(max_length=1000, null=True, verbose_name='\u0421\u043f\u0435\u0446\u0438\u0430\u043b\u0438\u0437\u0430\u0446\u0438\u044f', blank=True),
        ),
        migrations.AlterField(
            model_name='institution',
            name='main_photo',
            field=models.ImageField(upload_to=b'', null=True, verbose_name='\u0424\u043e\u0442\u043e', blank=True),
        ),
        migrations.AlterField(
            model_name='photo',
            name='photo',
            field=models.ImageField(upload_to=b'institution_photos', null=True, verbose_name='\u0424\u043e\u0442\u043e', blank=True),
        ),
        migrations.AlterField(
            model_name='service',
            name='title',
            field=models.CharField(max_length=255, null=True, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435', blank=True),
        ),
        migrations.AlterField(
            model_name='service',
            name='title_kk',
            field=models.CharField(max_length=255, null=True, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435', blank=True),
        ),
        migrations.AlterField(
            model_name='service',
            name='title_ru',
            field=models.CharField(max_length=255, null=True, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435', blank=True),
        ),
    ]
