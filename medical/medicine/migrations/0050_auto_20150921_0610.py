# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('medicine', '0049_auto_20150906_2136'),
    ]

    operations = [
        migrations.CreateModel(
            name='Banner',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=256, null=True, verbose_name='\u0417\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a', blank=True)),
                ('image', models.ImageField(upload_to=b'Banner/image', verbose_name='\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435')),
                ('url', models.URLField(null=True, verbose_name='\u0421\u0441\u044b\u043b\u043a\u0430', blank=True)),
                ('institution', models.ForeignKey(verbose_name='\u0417\u0430\u0432\u0435\u0434\u0435\u043d\u0438\u0435', blank=True, to='medicine.Institution', null=True)),
            ],
        ),
        migrations.CreateModel(
            name='PushUp',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('token', models.CharField(max_length=700, verbose_name='Token')),
                ('favorite', models.BooleanField(default=False, verbose_name='\u0418\u0437\u0431\u0440\u0430\u043d\u043d\u043e\u0435')),
                ('institution', models.ForeignKey(verbose_name='\u0417\u0430\u0432\u0435\u0434\u0435\u043d\u0438\u0435', to='medicine.Institution')),
            ],
        ),
        migrations.CreateModel(
            name='Specialty',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=256, verbose_name='\u0421\u043f\u0435\u0446\u0438\u0430\u043b\u044c\u043d\u043e\u0441\u0442\u044c')),
                ('logo', models.FileField(upload_to=b'Specialty/logo', verbose_name='\u041b\u043e\u0433\u043e\u0442\u0438\u043f')),
            ],
            options={
                'ordering': ['title'],
                'verbose_name': '\u0421\u043f\u0435\u0446\u0438\u0430\u043b\u044c\u043d\u043e\u0441\u0442\u044c',
                'verbose_name_plural': '\u0421\u043f\u0435\u0446\u0438\u0430\u043b\u044c\u043d\u043e\u0441\u0442\u0438',
            },
        ),
        migrations.AlterField(
            model_name='doctor',
            name='rel_institution',
            field=models.ForeignKey(blank=True, to='medicine.Institution', null=True),
        ),
        # migrations.AlterField(
        #     model_name='doctor',
        #     name='specs',
        #     field=models.CharField(max_length=255, null=True, verbose_name='\u0424\u043e\u0442\u043e', blank=True),
        # ),
        migrations.AlterField(
            model_name='doctor',
            name='specs_kk',
            field=models.CharField(max_length=255, null=True, verbose_name='\u0424\u043e\u0442\u043e', blank=True),
        ),
        migrations.AlterField(
            model_name='doctor',
            name='specs_ru',
            field=models.CharField(max_length=255, null=True, verbose_name='\u0424\u043e\u0442\u043e', blank=True),
        ),
        migrations.AddField(
            model_name='doctor',
            name='speciality',
            field=models.ForeignKey(verbose_name='\u0421\u043f\u0435\u0446\u0438\u0430\u043b\u044c\u043d\u043e\u0441\u0442\u044c', blank=True, to='medicine.Specialty', null=True),
        ),
    ]
