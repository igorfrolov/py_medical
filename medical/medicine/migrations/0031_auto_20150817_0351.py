# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('medicine', '0030_auto_20150817_0343'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='service',
            name='price_kk',
        ),
        migrations.RemoveField(
            model_name='service',
            name='price_ru',
        ),
    ]
