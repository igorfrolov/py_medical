# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('medicine', '0032_auto_20150817_0851'),
    ]

    operations = [
        migrations.RenameField(
            model_name='aboutproject',
            old_name='description_ky',
            new_name='description_kk',
        ),
        migrations.RenameField(
            model_name='action',
            old_name='description_ky',
            new_name='description_kk',
        ),
        migrations.RenameField(
            model_name='category',
            old_name='title_ky',
            new_name='title_kk',
        ),
        migrations.RenameField(
            model_name='doctor',
            old_name='name_ky',
            new_name='name_kk',
        ),
        migrations.RenameField(
            model_name='doctor',
            old_name='specs_ky',
            new_name='specs_kk',
        ),
        migrations.RenameField(
            model_name='institution',
            old_name='actions_ky',
            new_name='actions_kk',
        ),
        migrations.RenameField(
            model_name='institution',
            old_name='name_ky',
            new_name='name_kk',
        ),
        migrations.RenameField(
            model_name='institution',
            old_name='schedule_ky',
            new_name='schedule_kk',
        ),
        migrations.RenameField(
            model_name='institution',
            old_name='short_description_ky',
            new_name='short_description_kk',
        ),
        migrations.RenameField(
            model_name='service',
            old_name='title_ky',
            new_name='title_kk',
        ),
    ]
