# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('medicine', '0017_institution_telephone'),
    ]

    operations = [
        migrations.AlterField(
            model_name='institution',
            name='short_description',
            field=models.TextField(null=True, verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435'),
        ),
        migrations.AlterField(
            model_name='service',
            name='price',
            field=models.CharField(max_length=255, null=True, verbose_name='\u0426\u0435\u043d\u0430 \u0443\u0441\u043b\u0443\u0433\u0438', blank=True),
        ),
    ]
