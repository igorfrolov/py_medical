# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('medicine', '0055_remove_banner_internal_link'),
    ]

    operations = [
        migrations.AddField(
            model_name='banner',
            name='end',
            field=models.DateField(null=True, verbose_name='\u041a\u043e\u043d\u0435\u0446', blank=True),
        ),
        migrations.AddField(
            model_name='banner',
            name='start',
            field=models.DateField(null=True, verbose_name='\u041d\u0430\u0447\u0430\u043b\u043e', blank=True),
        ),
    ]
