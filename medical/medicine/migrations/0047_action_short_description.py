# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('medicine', '0046_auto_20150826_0354'),
    ]

    operations = [
        migrations.AddField(
            model_name='action',
            name='short_description',
            field=models.CharField(max_length=1000, null=True, verbose_name='\u041a\u043e\u0440\u043e\u0442\u043a\u043e\u0435 \u043e\u043f\u0438\u0441\u0430\u043d\u0438\u0435', blank=True),
        ),
    ]
