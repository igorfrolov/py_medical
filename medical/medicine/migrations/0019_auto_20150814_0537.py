# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('medicine', '0018_auto_20150814_0137'),
    ]

    operations = [
        # migrations.AlterField(
        #     model_name='institution',
        #     name='latitude',
        #     field=models.DecimalField(default=b'', verbose_name='\u0428\u0438\u0440\u043e\u0442\u0430', max_digits=13, decimal_places=6),
        # ),
        # migrations.AlterField(
        #     model_name='institution',
        #     name='longitude',
        #     field=models.DecimalField(default=b'', verbose_name='\u0414\u043e\u043b\u0433\u043e\u0442\u0430', max_digits=13, decimal_places=6),
        # ),
        migrations.AlterField(
            model_name='institution',
            name='short_description',
            field=models.TextField(null=True, verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435', blank=True),
        ),
    ]
