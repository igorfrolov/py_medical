# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('medicine', '0033_auto_20150817_0901'),
    ]

    operations = [
        migrations.AddField(
            model_name='institution',
            name='farm',
            field=models.BooleanField(default=False, verbose_name='\u0424\u0430\u0440\u043c \u043a\u043e\u043c\u043f\u0430\u043d\u0438\u044f'),
        ),
    ]
