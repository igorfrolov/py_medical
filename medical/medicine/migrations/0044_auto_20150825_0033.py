# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('medicine', '0043_institution_user'),
    ]

    operations = [
        migrations.AlterField(
            model_name='comment',
            name='institution',
            field=models.ForeignKey(related_name='comments', verbose_name='\u0417\u0430\u0432\u0435\u0434\u0435\u043d\u0438\u0435', to='medicine.Institution'),
        ),
    ]
