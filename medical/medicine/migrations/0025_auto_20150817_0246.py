# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('medicine', '0024_auto_20150817_0220'),
    ]

    operations = [
        migrations.AddField(
            model_name='institution',
            name='actions_kk',
            field=models.CharField(max_length=255, null=True, verbose_name='\u0410\u043a\u0446\u0438\u0438', blank=True),
        ),
        migrations.AddField(
            model_name='institution',
            name='actions_ru',
            field=models.CharField(max_length=255, null=True, verbose_name='\u0410\u043a\u0446\u0438\u0438', blank=True),
        ),
        migrations.AddField(
            model_name='institution',
            name='license_text_kk',
            field=models.CharField(max_length=255, null=True, verbose_name='\u041b\u0438\u0446\u0435\u043d\u0437\u0438\u044f'),
        ),
        migrations.AddField(
            model_name='institution',
            name='license_text_ru',
            field=models.CharField(max_length=255, null=True, verbose_name='\u041b\u0438\u0446\u0435\u043d\u0437\u0438\u044f'),
        ),
        migrations.AddField(
            model_name='institution',
            name='name_kk',
            field=models.CharField(max_length=255, null=True, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435'),
        ),
        migrations.AddField(
            model_name='institution',
            name='name_ru',
            field=models.CharField(max_length=255, null=True, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435'),
        ),
        migrations.AddField(
            model_name='institution',
            name='schedule_kk',
            field=models.CharField(max_length=255, null=True, verbose_name='\u0420\u0430\u0441\u043f\u0438\u0441\u0430\u043d\u0438\u0435'),
        ),
        migrations.AddField(
            model_name='institution',
            name='schedule_ru',
            field=models.CharField(max_length=255, null=True, verbose_name='\u0420\u0430\u0441\u043f\u0438\u0441\u0430\u043d\u0438\u0435'),
        ),
        migrations.AddField(
            model_name='institution',
            name='short_description_kk',
            field=models.TextField(null=True, verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435', blank=True),
        ),
        migrations.AddField(
            model_name='institution',
            name='short_description_ru',
            field=models.TextField(null=True, verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435', blank=True),
        ),
    ]
