# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('medicine', '0021_auto_20150814_0700'),
    ]

    operations = [
        migrations.AddField(
            model_name='institution',
            name='latitude',
            field=models.FloatField(default=0, null=True, verbose_name='\u0428\u0438\u0440\u043e\u0442\u0430'),
        ),
        migrations.AddField(
            model_name='institution',
            name='longitude',
            field=models.FloatField(default=0, null=True, verbose_name='\u0414\u043e\u043b\u0433\u043e\u0442\u0430'),
        ),
    ]
