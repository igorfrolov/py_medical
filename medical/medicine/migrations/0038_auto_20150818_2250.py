# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('medicine', '0037_auto_20150818_2245'),
    ]

    operations = [
        migrations.RenameField(
            model_name='service',
            old_name='description',
            new_name='full_description',
        ),
        migrations.RenameField(
            model_name='service',
            old_name='description_kk',
            new_name='full_description_kk',
        ),
        migrations.RenameField(
            model_name='service',
            old_name='description_ru',
            new_name='full_description_ru',
        ),
    ]
