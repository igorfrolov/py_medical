# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('medicine', '0053_remove_banner_url'),
    ]

    operations = [
        migrations.AddField(
            model_name='banner',
            name='categories',
            field=models.ManyToManyField(related_name='banners', null=True, verbose_name='\u041a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u0438', to='medicine.Category', blank=True),
        ),
        migrations.AddField(
            model_name='banner',
            name='external_link',
            field=models.CharField(max_length=300, null=True, verbose_name='\u0412\u043d\u0435\u0448\u043d\u044f\u044f \u0441\u0441\u044b\u043b\u043a\u0430', blank=True),
        ),
        migrations.AddField(
            model_name='banner',
            name='internal_link',
            field=models.CharField(max_length=300, null=True, verbose_name='\u0412\u043d\u0443\u0442\u0440\u0435\u043d\u043d\u044f\u044f \u0441\u0441\u044b\u043b\u043a\u0430', blank=True),
        ),
    ]
