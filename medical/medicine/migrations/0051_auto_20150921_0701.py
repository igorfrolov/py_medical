# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('medicine', '0050_auto_20150921_0610'),
    ]

    operations = [
        # migrations.RemoveField(
        #     model_name='doctor',
        #     name='specs',
        # ),
        migrations.RemoveField(
            model_name='doctor',
            name='specs_kk',
        ),
        migrations.RemoveField(
            model_name='doctor',
            name='specs_ru',
        ),
    ]
