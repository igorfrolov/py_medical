# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('medicine', '0016_doctor_email'),
    ]

    operations = [
        migrations.AddField(
            model_name='institution',
            name='telephone',
            field=models.CharField(max_length=500, verbose_name='\u0422\u0435\u043b\u0435\u0444\u043e\u043d\u044b', blank=True),
        ),
    ]
