# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('medicine', '0031_auto_20150817_0351'),
    ]

    operations = [
        migrations.RenameField(
            model_name='aboutproject',
            old_name='description_kk',
            new_name='description_ky',
        ),
        migrations.RenameField(
            model_name='action',
            old_name='description_kk',
            new_name='description_ky',
        ),
        migrations.RenameField(
            model_name='category',
            old_name='title_kk',
            new_name='title_ky',
        ),
        migrations.RenameField(
            model_name='doctor',
            old_name='name_kk',
            new_name='name_ky',
        ),
        migrations.RenameField(
            model_name='doctor',
            old_name='specs_kk',
            new_name='specs_ky',
        ),
        migrations.RenameField(
            model_name='institution',
            old_name='actions_kk',
            new_name='actions_ky',
        ),
        migrations.RenameField(
            model_name='institution',
            old_name='name_kk',
            new_name='name_ky',
        ),
        migrations.RenameField(
            model_name='institution',
            old_name='schedule_kk',
            new_name='schedule_ky',
        ),
        migrations.RenameField(
            model_name='institution',
            old_name='short_description_kk',
            new_name='short_description_ky',
        ),
        migrations.RenameField(
            model_name='service',
            old_name='title_kk',
            new_name='title_ky',
        ),
    ]
