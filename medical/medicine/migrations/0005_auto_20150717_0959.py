# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('medicine', '0004_merge'),
    ]

    operations = [
        migrations.AlterField(
            model_name='institution',
            name='price_list_file',
            field=models.FileField(default=None, upload_to=b'', verbose_name=b'\xd0\xa4\xd0\xb0\xd0\xb9\xd0\xbb', blank=True),
        ),
    ]
