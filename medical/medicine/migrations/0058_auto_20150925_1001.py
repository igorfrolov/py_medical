# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('medicine', '0057_banner_deactive'),
    ]

    operations = [
        migrations.AddField(
            model_name='banner',
            name='external',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='banner',
            name='internal',
            field=models.BooleanField(default=False),
        ),
    ]
