# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('medicine', '0014_institution_main_photo'),
    ]

    operations = [
        migrations.RenameField(
            model_name='service',
            old_name='institution',
            new_name='rel_institution',
        ),
    ]
