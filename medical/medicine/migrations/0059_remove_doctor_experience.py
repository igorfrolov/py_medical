# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('medicine', '0058_auto_20150925_1001'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='doctor',
            name='experience',
        ),
    ]
