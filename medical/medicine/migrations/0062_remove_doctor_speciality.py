# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('medicine', '0061_auto_20150929_0913'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='doctor',
            name='speciality',
        ),
    ]
