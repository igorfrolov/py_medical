# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('medicine', '0039_price'),
    ]

    operations = [
        migrations.AlterField(
            model_name='doctor',
            name='doctor_photo',
            field=models.ImageField(upload_to=b'doctors_photos', verbose_name='\u0424\u043e\u0442\u043e', blank=True),
        ),
        migrations.AlterField(
            model_name='institution',
            name='logo',
            field=models.ImageField(upload_to=b'logos', null=True, verbose_name='\u041b\u043e\u0433\u043e\u0442\u0438\u043f', blank=True),
        ),
        migrations.AlterField(
            model_name='photo',
            name='photo',
            field=models.ImageField(upload_to=b'institution_photos', verbose_name='\u0424\u043e\u0442\u043e'),
        ),
    ]
