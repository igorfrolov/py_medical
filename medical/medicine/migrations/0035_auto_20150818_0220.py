# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('medicine', '0034_institution_farm'),
    ]

    operations = [
        migrations.AlterField(
            model_name='institution',
            name='license_photo',
            field=models.ImageField(upload_to=b'', null=True, verbose_name='\u0424\u043e\u0442\u043e \u043b\u0438\u0446\u0435\u043d\u0437\u0438\u0438', blank=True),
        ),
        migrations.AlterField(
            model_name='institution',
            name='license_text',
            field=models.CharField(max_length=255, null=True, verbose_name='\u041b\u0438\u0446\u0435\u043d\u0437\u0438\u044f'),
        ),
    ]
