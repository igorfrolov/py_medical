# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('medicine', '0013_auto_20150811_2248'),
    ]

    operations = [
        migrations.AddField(
            model_name='institution',
            name='main_photo',
            field=models.ImageField(upload_to=b'', verbose_name='\u0424\u043e\u0442\u043e', blank=True),
        ),
    ]
