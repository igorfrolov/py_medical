# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('medicine', '0045_auto_20150826_0246'),
    ]

    operations = [
        migrations.AlterField(
            model_name='category',
            name='background',
            field=models.ImageField(upload_to=b'category_backgrounds', null=True, verbose_name='\u0424\u043e\u043d\u043e\u0432\u043e\u0435 \u0438\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435', blank=True),
        ),
        migrations.AlterField(
            model_name='category',
            name='logo',
            field=models.ImageField(upload_to=b'category_logos', null=True, verbose_name='\u041b\u043e\u0433\u043e\u0442\u0438\u043f', blank=True),
        ),
    ]
