from django.shortcuts import HttpResponseRedirect
from django.core.urlresolvers import reverse


def inst_created(sender, instance, created, **kwargs):
    if instance.farm:
        HttpResponseRedirect(reverse('import_data', instance.id))