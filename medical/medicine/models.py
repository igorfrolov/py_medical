# -*- coding: utf-8 -*
import os
from django.db import models
from django.conf import settings
from django_resized import ResizedImageField
from django.core.validators import RegexValidator
from django.utils.translation import ugettext as _
from django.db.models.signals import post_save
import signals as inst_signals
from pytils import translit


def get_image_path(self, filename):
    ext = filename.split('.')[-1]
    path = os.path.join("category_logos", "%s.%s" % (translit.slugify(filename), ext))
    return path


def get_bg_path(self, filename):
    ext = filename.split('.')[-1]
    path = os.path.join("category_backgrounds", "%s.%s" % (translit.slugify(filename), ext))
    return path


class Category(models.Model):
    title = models.CharField(max_length=255, verbose_name=_(u"Название"), blank=True, null=True)
    logo = models.ImageField(upload_to=get_image_path, verbose_name=_(u"Логотип"), blank=True, null=True)
    background = models.ImageField(upload_to=get_bg_path, verbose_name=_(u"Фоновое изображение"), blank=True, null=True)

    class Meta:
        verbose_name = _(u"Категория")
        verbose_name_plural = _(u"Категории")

    def __unicode__(self):
        return self.title


def get_logos_path(self, filename):
    ext = filename.split('.')[-1]
    path = os.path.join("logos", "%s.%s" % (translit.slugify(filename), ext))
    return path


def get_main_photo_path(self, filename):
    ext = filename.split('.')[-1]
    path = "%s.%s" % (translit.slugify(filename), ext)
    return path


def get_license_photo_path(self, filename):
    ext = filename.split('.')[-1]
    path = "%s.%s" % (translit.slugify(filename), ext)
    return path


class Institution(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name=_(u'Юзер'), blank=True, null=True)
    category = models.ManyToManyField(Category, verbose_name=_(u"Категория"), blank=True, related_name="categories", null=True)
    logo = models.ImageField(upload_to=get_logos_path,
                             verbose_name=_(u"Логотип"), blank=True, null=True)
    main_photo = models.ImageField(blank=True, verbose_name=_(u"Фото"), null=True, upload_to=get_main_photo_path)
    name = models.CharField(max_length=255, verbose_name=_(u"Название"), blank=True, null=True)
    short_description = models.TextField(verbose_name=_(u"Описание"), blank=True, null=True)
    license_text = models.CharField(max_length=255, blank=True, verbose_name=_(u"Лицензия"), null=True)
    license_photo = models.ImageField(blank=True, verbose_name=_(u"Фото лицензии"), null=True, upload_to=get_license_photo_path)
    actions = models.CharField(max_length=255, verbose_name=_(u"Акции"), blank=True, null=True)
    schedule = models.CharField(max_length=255, verbose_name=_(u"Расписание"), blank=True, null=True)
    telephone = models.CharField(max_length=500, verbose_name=_(u"Телефоны"), blank=True, null=True)
    email = models.EmailField(max_length=255, verbose_name=_(u"Почта"), blank=True, null=True)
    address = models.CharField(max_length=255, verbose_name=_(u"Адрес"), blank=True, null=True)
    longitude = models.FloatField(verbose_name=_(u"Долгота"),  default=0, null=True, blank=True)
    latitude = models.FloatField(verbose_name=_(u"Широта"), default=0, null=True, blank=True)
    website = models.CharField(max_length=255, verbose_name=_(u"Сайт"), blank=True, null=True)
    farm = models.BooleanField(verbose_name=_(u'Фарм компания'), default=False)
    is_active = models.BooleanField(verbose_name=_(u'Активное заведение'), default=True)

    class Meta:
        verbose_name = _(u"Заведение")
        verbose_name_plural = _(u"Заведения")

    def __unicode__(self):
        return self.name


class Phone(models.Model):
    phone = phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$')
    phone_number = models.CharField(max_length=13, validators=[phone_regex],
                                    verbose_name=_(u"Телефон (+996XXXXXXXXX)"), blank=True, null=True)
    rel_institution = models.ForeignKey(Institution)

    class Meta:
        verbose_name = _(u"Телефон")
        verbose_name_plural = _(u"Телефоны")


def get_photo_path(self, filename):
    ext = filename.split('.')[-1]
    path = os.path.join("institution_photos", "%s.%s" % (translit.slugify(filename), ext))
    return path


class Photo(models.Model):
    photo = models.ImageField(upload_to=get_photo_path,
                              verbose_name=_(u"Фото"), blank=True, null=True)
    description = models.CharField(verbose_name=_(u'Описание'), max_length=500, blank=True, null=True)
    rel_institution = models.ForeignKey(Institution)

    class Meta:
        verbose_name = _(u"Фото")
        verbose_name_plural = _(u"Фото")


def get_logo_spec_path(self, filename):
    ext = filename.split('.')[-1]
    path = os.path.join("Specialty", "logo", "%s.%s" % (translit.slugify(filename), ext))
    return path


class Specialty(models.Model):
    title = models.CharField(verbose_name=_(u'Специальность'), max_length=256, blank=True, null=True)
    logo = models.FileField(verbose_name=u'Логотип', upload_to=get_logo_spec_path, blank=True, null=True)

    def __unicode__(self):
        return self.title

    class Meta:
        ordering = ['title']
        verbose_name = _(u'Специальность')
        verbose_name_plural = _(u'Специальности')


def get_doctor_photo_path(self, filename):
    ext = filename.split('.')[-1]
    path = os.path.join("doctors_photos", "%s.%s" % (translit.slugify(filename), ext))
    return path


class Doctor(models.Model):
    specialties = models.ManyToManyField(Specialty, verbose_name=_(u'Специальность'), blank=True, null=True)
    name = models.CharField(max_length=255, verbose_name=_(u"ФИО"), blank=True, null=True)
    doctor_photo = models.ImageField(upload_to=get_doctor_photo_path,
                                     verbose_name=_(u"Фото"), blank=True, null=True)
    description = models.TextField(verbose_name=_(u'Описание'), blank=True, null=True)
    regime = models.CharField(max_length=255, default="", verbose_name=_(u"Режим работы"), blank=True, null=True)
    phone_number = models.CharField(max_length=13, blank=True, null=True,
                                    verbose_name=_(u"Телефон (+996XXXXXXXXX)"))
    email = models.EmailField(verbose_name=u"Email", blank=True, null=True)
    doctor_to_home = models.BooleanField(verbose_name=_(u"Возможность вызова на дом"), default=False)
    rel_institution = models.ForeignKey(Institution, blank=True, null=True)

    class Meta:
        verbose_name = _(u"Врач")
        verbose_name_plural = _(u"Врачи")

    def __unicode__(self):
        return self.name


class Service(models.Model):
    rel_institution = models.ForeignKey(Institution)
    title = models.CharField(max_length=255, verbose_name=_(u"Название"), blank=True, null=True)
    full_description = models.TextField(verbose_name=_(u'Полное описание'), blank=True, null=True)
    price = models.CharField(max_length=255, verbose_name=_(u"Цена услуги"), blank=True, null=True)

    class Meta:
        verbose_name = _(u"Услуга")
        verbose_name_plural = _(u"Услуги")

    def __unicode__(self):
        return self.title


class Comment(models.Model):
    institution = models.ForeignKey(Institution, verbose_name=_(u"Заведение"), related_name='comments')
    commentator_name = models.CharField(max_length=255, verbose_name=_(u"Имя"))
    comment = models.TextField(verbose_name=_(u"Комментарий"))
    public = models.BooleanField(verbose_name=_(u"Опубликовать"), default=False)

    class Meta:
        verbose_name = _(u"Отзыв")
        verbose_name_plural = _(u"Отзывы")

    def __unicode__(self):
        return self.comment


class AboutProject(models.Model):
    description = models.TextField(verbose_name=_(u'Описание проекта'))

    class Meta:
        verbose_name = _(u'Информация о проекте')
        verbose_name_plural = _(u'Информация о проекте')


class Contact(models.Model):
    phone = models.CharField(verbose_name=_(u'Телефон'), max_length=13, blank=True, null=True)
    email = models.EmailField(verbose_name=_(u'Почта'), max_length=128, blank=True, null=True)
    address = models.CharField(verbose_name=_(u'Адрес'), max_length=2048, blank=True, null=True)

    def __unicode__(self):
        return self.address

    class Meta:
        verbose_name = _(u'Контакт')
        verbose_name_plural = _(u'Контакты')


class Feedback(models.Model):
    name = models.CharField(max_length=256, verbose_name=_(u'Имя'))
    email = models.CharField(verbose_name=_(u'Почта'), max_length=128, null=True)
    phone = models.CharField(verbose_name=_(u'Телефон'), max_length=13)
    message = models.TextField(verbose_name=_(u'Сообщение'))
    created_at = models.DateTimeField(verbose_name=_(u'Дата создания'), auto_now_add=True)

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = _(u'Обратная связь')
        verbose_name_plural = _(u'Обратная связь')


def get_action_photos_path(self, filename):
    ext = filename.split('.')[-1]
    path = os.path.join("action_photos", "%s.%s" % (translit.slugify(filename), ext))
    return path


class Action(models.Model):
    institution = models.ForeignKey(Institution)
    short_description = models.CharField(verbose_name=u'Короткое описание', max_length=1000, blank=True, null=True)
    description = models.TextField(verbose_name=_(u'Описание акции'), blank=True, null=True)
    photo = models.ImageField(upload_to=get_action_photos_path, verbose_name=_(u"Фото для акции"), blank=True, null=True)

    def __unicode__(self):
        return self.description or u''

    class Meta:
        verbose_name = _(u'Акция')
        verbose_name_plural = _(u'Акции')


class Price(models.Model):
    institution = models.ForeignKey(Institution)
    title = models.CharField(verbose_name=_(u'Название'), max_length=500)
    price = models.CharField(verbose_name=_(u'Цена'), max_length=500)

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = _(u'Прайс')
        verbose_name_plural = _(u'Прайсы')


class PushUp(models.Model):
    institution = models.ForeignKey(Institution, verbose_name=u'Заведение')
    token = models.CharField(verbose_name=u'Token', max_length=700)
    favorite = models.BooleanField(verbose_name=u'Избранное', default=False)

    def __unicode__(self):
        return self.token


def get_banner_photos_path(self, filename):
        ext = filename.split('.')[-1]
        path = "Banner\image\%s.%s" % (translit.slugify(filename), ext)
        return path


class Banner(models.Model):
    institution = models.ForeignKey(Institution, verbose_name=u'Заведение', blank=True, null=True)
    title = models.CharField(verbose_name=_(u'Заголовок'), max_length=256, blank=True, null=True)
    image = models.ImageField(verbose_name=_(u'Изображение'), upload_to=get_banner_photos_path)
    external_link = models.CharField(verbose_name=_(u'Внешняя ссылка'), max_length=300, blank=True, null=True)
    categories = models.ManyToManyField(Category, verbose_name=_(u"Категории"), blank=True, null=True, related_name="banners")
    start = models.DateField(verbose_name=_(u'Начало'), blank=True, null=True)
    end = models.DateField(verbose_name=_(u'Конец'), blank=True, null=True)
    deactive = models.BooleanField(default=False)
    external = models.BooleanField(default=False)
    internal = models.BooleanField(default=False)

    def __unicode__(self):
        return self.title
