# -*- coding: utf-8 -*-
from django import forms
from django.core.files.images import get_image_dimensions
from django.forms import ModelForm, DateField
from django.forms.widgets import CheckboxSelectMultiple
from django.utils.translation import ugettext as _
from medicine.models import Institution, Category, Comment, Doctor, Photo, Phone, Feedback, AboutProject, Contact, \
    Action, \
    Service, Price, Specialty, Banner
from django.forms.models import inlineformset_factory
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Fieldset, HTML, ButtonHolder, Submit
from datetimewidget.widgets import DateWidget
from django.contrib.admin.widgets import AdminDateWidget


class InstitutionForm(forms.ModelForm):
    class Meta:
        model = Institution
        fields = ['user', 'category', 'logo', 'main_photo', 'name_ru', 'name_kk', 'short_description_ru',
                  'short_description_kk', 'license_text', 'license_photo', 'actions', 'schedule', 'telephone', 'email',
                  'address', 'longitude', 'latitude', 'website', 'farm', 'is_active']
        exclude = ('actions',)
        error_css_class = 'error'
        required_css_class = 'required'

    def __init__(self, *args, **kwargs):
        super(InstitutionForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        price = _(u'Прайс лист')
        services = _(u'Услуги')
        photos = _(u'Фотографии')
        phones = _(u'Телефоны')
        doctors = _(u'Врачи')
        actions = _(u'Акции')
        action_error = _(u'Загружаемое изображение превышает 700px х 700px')
        photo_error = _(u'Загружаемое изображение превышает 700px х 700px')
        doctor_error = _(u'Загружаемое изображение превышает 700px х 700px')
        self.helper.form_method = 'post'
        self.helper.layout = Layout(
            Fieldset('',
                     'user',
                     'category',
                     'farm',
                     'logo',
                     'main_photo',
                     'name_ru',
                     'name_kk',
                     'short_description_ru',
                     'short_description_kk',

                     HTML(u'<fieldset id="id_service">\
                        <legend>' + services + u'</legend>\
                        {{ service_form.management_form }}\
                        {{ service_form.non_form_errors }}\
                        {% for form in service_form %}\
                            {{ form.id }}\
                            <div class="inline {{ service_form.prefix }}">\
                                {{ form.label_tag }}\
                                {{ form.as_p }}<br/>\
                            </div>\
                        {% endfor %}\
                    </fieldset>'),
                     HTML(u'<fieldset id="id_photo">\
                        <legend>' + photos + u'</legend>\
                        {{ photo_form.management_form }}\
                        {{ photo_form.non_form_errors }}\
                        {% for form in photo_form %}\
                            {{ form.id }}\
                            <div class="inline {{ photo_form.prefix }}">\
                                {{ form.label_tag }}\
                                {% if action_form.errors %}\
                                    <h4>' + photo_error + u'</h4>\
                                {% endif %}\
                                {{ form.as_p }}\
                            </div>\
                        {% endfor %}\
                    </fieldset>'),
                     'license_text',
                     'license_photo',
                     'schedule',
                     'telephone',
                     'email',
                     'address',
                     'longitude',
                     'latitude',
                     'website',
                     # 'price_list_desc',
                     # 'price_list_file',
                     HTML(u'<fieldset id="id_doctors">\
                        <legend>' + doctors + u'</legend>\
                        {{ doctor_form.management_form }}\
                        {{ doctor_form.non_form_errors }}\
                        {% for form in doctor_form %}\
                            {{ form.id }}\
                            <div class="inline {{ doctor_form.prefix }}">\
                                {{ form.label_tag }}\
                                {% if action_form.errors %}\
                                    <h4>' + doctor_error + u'</h4>\
                                {% endif %}\
                                {{ form.as_p }}\
                            </div>\
                        {% endfor %}\
                    </fieldset>'),
                     HTML(u'<fieldset id="id_actions">\
                        <legend>' + actions + u'</legend>\
                        {{ action_form.management_form }}\
                        {{ action_form.non_form_errors }}\
                        {% for form in action_form %}\
                            {{ form.id }}\
                            <div class="inline {{ action_form.prefix }}">\
                                {{ form.label_tag }}\
                                {% if action_form.errors %}\
                                    <h2>' + action_error + u'</h2>\
                                {% endif %}\
                                {{ form.as_p }}\
                            </div>\
                        {% endfor %}\
                    </fieldset>'),
            ),
            ButtonHolder(
                Submit('submit', _(u'Сохранить'), css_class='btn btn-default')
            )

        )


class CategoryForm(forms.ModelForm):
    class Meta:
        model = Category
        fields = ['title_ru', 'title_kk', 'logo', 'background']
        error_css_class = 'error'
        required_css_class = 'required'

    def clean_logo(self):
        logo = self.cleaned_data.get("logo")
        if not logo:
            raise forms.ValidationError(_(u"Нет фото"))
        else:
            w, h = get_image_dimensions(logo)
            if w > 700:
                raise forms.ValidationError(_(u"Загружаемая ширина изображение %i px. Разрешённая ширина 700px") % w)
            if h > 700:
                raise forms.ValidationError(_(u"Загружаемая высота изображение %i px. Разрешённая высота 700px") % h)
        return logo


class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = '__all__'
        exclude = ['institution']
        error_css_class = 'error'
        required_css_class = 'required'


class ActionForm(ModelForm):
    class Meta:
        model = Action
        fields = ['short_description_ru', 'short_description_kk', 'description_ru', 'description_kk', 'photo']
        exclude = ['institution']
        widgets = {
            'description_ru': forms.Textarea(attrs={'class': 'summernote'}),
            'description_kk': forms.Textarea(attrs={'class': 'summernote'})
        }

    def clean_photo(self):
        photo = self.cleaned_data.get("photo")
        if not photo:
            return None
        else:
            w, h = get_image_dimensions(photo)
            if w > 700:
                raise forms.ValidationError(_(u"Загружаемая ширина изображение %i px. Разрешённая ширина 700px") % w)
            if h > 700:
                raise forms.ValidationError(_(u"Загружаемая высота изображение %i px. Разрешённая высота 700px") % h)
        return photo


class ServiceForm(ModelForm):
    class Meta:
        model = Service
        fields = ['title_ru', 'title_kk', 'full_description_ru', 'full_description_kk', 'price']
        exclude = ['rel_institution']
        widgets = {
            'full_description_ru': forms.Textarea(attrs={'class': 'summernote'}),
            'full_description_kk': forms.Textarea(attrs={'class': 'summernote'})
        }


class PhotoForm(ModelForm):
    class Meta:
        model = Photo
        fields = '__all__'

    def clean_photo(self):
        photo = self.cleaned_data.get("photo")
        if not photo:
            raise forms.ValidationError(_(u"Нет фото"))
        else:
            w, h = get_image_dimensions(photo)
            if w > 700:
                raise forms.ValidationError(_(u"Загружаемая ширина изображение %i px. Разрешённая ширина 600px") % w)
            if h > 700:
                raise forms.ValidationError(_(u"Загружаемая высота изображение %i px. Разрешённая высота 600px") % h)
        return photo


class DoctorForm(ModelForm):
    class Meta:
        model = Doctor
        fields = ['name_ru', 'name_kk', 'doctor_photo', 'description', 'regime', 'phone_number', 'email', 'specialties',
                  'doctor_to_home']

    def clean_photo(self):
        doctor_photo = self.cleaned_data.get("doctor_photo")
        if not doctor_photo:
            raise forms.ValidationError(_(u"Нет фото"))
        else:
            w, h = get_image_dimensions(doctor_photo)
            if w > 700:
                raise forms.ValidationError(_(u"Загружаемая ширина изображение %i px. Разрешённая ширина 600px") % w)
            if h > 700:
                raise forms.ValidationError(_(u"Загружаемая высота изображение %i px. Разрешённая высота 600px") % h)
        return doctor_photo


InstitutionPhotosFromSet = inlineformset_factory(Institution, Photo, form=PhotoForm, fields='__all__', extra=1)
InstitutionDoctorsFromSet = inlineformset_factory(Institution, Doctor, form=DoctorForm,
                                                  fields=['name_ru', 'name_kk', 'doctor_photo', 'description', 'regime',
                                                          'phone_number', 'email', 'specialties',
                                                          'doctor_to_home'], extra=1)
InstitutionActionFromSet = inlineformset_factory(Institution, Action, form=ActionForm,
                                                 fields=['short_description_ru', 'short_description_kk',
                                                         'description_ru', 'description_kk', 'photo'], extra=1)

InstitutionServicesFromSet = inlineformset_factory(Institution, Service, form=ServiceForm,
                                                   fields=['title_ru', 'title_kk', 'full_description_ru',
                                                           'full_description_kk', 'price'], extra=1)

# InstitutionPriceFormset = inlineformset_factory(Institution, Price, fields=['file', 'farm'], extra=1, max_num=1)
# InstitutionPhonesFromSet = inlineformset_factory(Institution, Phone, fields='__all__', extra=1)


class FeedbackForm(forms.ModelForm):
    class Meta:
        model = Feedback
        fields = ['name', 'email', 'phone', 'message']
        error_css_class = 'error'
        required_css_class = 'required'


class AboutProjectForm(forms.ModelForm):
    class Meta:
        model = AboutProject
        fields = ['description_kk', 'description_ru']
        error_css_class = 'error'
        required_css_class = 'required'


class ContactForm(forms.ModelForm):
    class Meta:
        model = Contact
        fields = ['phone', 'email', 'address']
        error_css_class = 'error'
        required_css_class = 'required'

    def __init__(self, *args, **kwargs):
        super(ContactForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.layout = Layout(
            Fieldset('',
                     'phone',
                     'email',
                     'address',
            ),
            ButtonHolder(
                Submit('submit', _(u'Сохранить'), css_class='btn btn-default')
            )

        )


class ServiceForm(ModelForm):
    class Meta:
        model = Service
        fields = ['title_ru', 'title_kk', 'full_description_ru', 'full_description_kk', 'price']
        exclude = ['rel_institution']


import os

IMPORT_FILE_TYPES = ['.xls', '.xlsx']


class UploadFileForm(forms.Form):
    file = forms.FileField()
    # farm = forms.ModelChoiceField(queryset=Institution.objects.filter(farm=True))

    def clean(self):
        data = super(UploadFileForm, self).clean()
        if 'file' not in data:
            raise forms.ValidationError(_(u'Вы забыли загрузить файл Excel'))
        file = data['file']
        extension = os.path.splitext(file.name)[1]
        if not (extension in IMPORT_FILE_TYPES):
            raise forms.ValidationError(_(u'%s не тот формат') % file.name)
            errorrr = forms.ValidationError


class DoctorsForm(ModelForm):
    class Meta:
        model = Doctor
        fields = ['rel_institution', 'name_ru', 'name_kk', 'doctor_photo', 'description', 'regime', 'phone_number',
                  'email', 'specialties', 'doctor_to_home']

    def clean_photo(self):
        doctor_photo = self.cleaned_data.get("doctor_photo")
        if not doctor_photo:
            raise forms.ValidationError(_(u"Нет фото"))
        else:
            w, h = get_image_dimensions(doctor_photo)
            if w > 700:
                raise forms.ValidationError(_(u"Загружаемая ширина изображение %i px. Разрешённая ширина 600px") % w)
            if h > 700:
                raise forms.ValidationError(_(u"Загружаемая высота изображение %i px. Разрешённая высота 600px") % h)
        return doctor_photo


class SpecialtyForm(ModelForm):
    class Meta:
        model = Specialty
        fields = ['title', 'logo']


class BannerForm(ModelForm):
    class Meta:
        model = Banner
        fields = ['title', 'image', 'start', 'end', 'institution', 'external_link', 'categories', 'external',
                  'internal']
        widgets = {
            'start': DateWidget(attrs={'id': "id_start"}, usel10n=True, bootstrap_version=3),
            'end': DateWidget(attrs={'id': "id_end"}, usel10n=True, bootstrap_version=3)
        }

    def clean_iamge(self):
        image = self.cleaned_data.get("image")
        if not image:
            raise forms.ValidationError(_(u"Нет фото"))
        else:
            w, h = get_image_dimensions(image)
            if w > 2000:
                raise forms.ValidationError(_(u"Загружаемая ширина изображение %i px. Разрешённая ширина 600px") % w)
            if h > 2000:
                raise forms.ValidationError(_(u"Загружаемая высота изображение %i px. Разрешённая высота 600px") % h)
        return image

    def __init__(self, *args, **kwargs):

        super(BannerForm, self).__init__(*args, **kwargs)

        self.fields["categories"].widget = CheckboxSelectMultiple()
        self.fields["categories"].queryset = Category.objects.all()

    #
    # def __init__(self, *args, **kwargs):
    #     super(BannerForm, self).__init__(*args, **kwargs)
    #     self.fields['categories'].label = 'Category'