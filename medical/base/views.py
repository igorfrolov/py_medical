# -*- coding: utf-8 -*-

from gcm import GCM
from django.shortcuts import render, get_object_or_404, redirect
from django.contrib import auth
from django.core.context_processors import csrf
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth.decorators import user_passes_test
from django.conf import settings

from forms import *
from medicine.models import Institution, Category, Comment, Feedback, AboutProject, Contact, Action, Service, Doctor, Specialty, PushUp, Banner
from .decorators import admin_required
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage


@admin_required
def home(request, template='base/home.html'):
    """ Default view for the root """
    full_name = request.user.username
    return render(request, template, locals())


def login(request):
    c = {}
    c.update(csrf(request))
    return render(request, 'base/login.html', c)


def auth_view(request):
    # TODO: Use forms instead of this vulnerable code
    username = request.POST.get('username', '')
    password = request.POST.get('password', '')
    user = auth.authenticate(username=username, password=password)
    if user is not None:
        if user.is_active:
            auth.login(request, user)
            return redirect('base:logged_in')
        else:
            return redirect('base:home')
    else:
        return redirect('base:home')


def logged_in(request):
    return redirect('base:home')


def logout(request):
    auth.logout(request)
    return redirect('base:login')


def chunks(l, block_size):
    """
    Процедура режет массив на куски
    :param l: масиив
    :param block_size: размер куска
    :return:
    """
    for i in xrange(0, len(l), block_size):
        yield l[i:i+block_size]


def send_pushes_for_tokens(tokens, data):
    gcm = GCM(settings.GCM_KEY)

    for t in chunks(tokens, 1000):
        gcm.json_request(registration_ids=t, data=data)


def send_pushes_for_topic(data):
    gcm = GCM(settings.GCM_KEY)

    gcm.send_topic_message(topic='global', data=data)


@admin_required
@user_passes_test(lambda u: u.is_superuser)
def institution_create(request):
    if request.POST:
        form = InstitutionForm(request.POST, request.FILES)
        service_form = InstitutionServicesFromSet(request.POST, request.FILES)
        photo_form = InstitutionPhotosFromSet(request.POST, request.FILES)
        price_form = UploadFileForm(request.POST, request.FILES)
        doctor_form = InstitutionDoctorsFromSet(request.POST, request.FILES)
        # phone_form = InstitutionPhonesFromSet(request.POST, request.FILES)
        action_form = InstitutionActionFromSet(request.POST, request.FILES)
        if form.is_valid() and service_form.is_valid() and doctor_form.is_valid() and photo_form.is_valid() \
                and action_form.is_valid():
            obj = form.save()
            service_form.instance = form.save()
            service_form.save()
            photo_form.instance = form.save()
            photo_form.save()
            doctor_form.instance = form.save()
            doctor_form.save()
            # phone_form.instance = form.save()
            # phone_form.save()
            action_form.instance = form.save()
            action_form.save()

            if obj.is_active:
                data = {
                    'message': u'Добавлено заведение',
                    'obj': obj.name,
                    'obj_id': obj.id,
                    'message_two': u''
                }

                send_pushes_for_topic(data=data)

            if obj.farm:
                return HttpResponseRedirect('/ru/upload/%s/' % obj.pk)
            else:
                return redirect('base:institutions_list')
    else:
        form = InstitutionForm()
        service_form = InstitutionServicesFromSet()
        photo_form = InstitutionPhotosFromSet()
        doctor_form = InstitutionDoctorsFromSet()
        # phone_form = InstitutionPhonesFromSet()
        action_form = InstitutionActionFromSet()

    args = {}
    args.update(csrf(request))

    args['form'] = form
    args['service_form'] = service_form
    args['photo_form'] = photo_form
    args['doctor_form'] = doctor_form
    # args['phone_form'] = phone_form
    args['action_form'] = action_form
    full_name = request.user.username

    return render(request, 'base/create_institution.html', locals())


@admin_required
@user_passes_test(lambda u: u.is_superuser)
def institution_update(request, institution_id, template='base/update_institution.html'):
    instance = get_object_or_404(Institution, id=institution_id)
    instance_is_active = instance.is_active
    form = InstitutionForm(request.POST or None, request.FILES or None, instance=instance)
    service_form = InstitutionServicesFromSet(request.POST or None, request.FILES or None, instance=instance)
    price_form = UploadFileForm(request.POST, request.FILES)
    photo_form = InstitutionPhotosFromSet(request.POST or None, request.FILES or None, instance=instance)
    doctor_form = InstitutionDoctorsFromSet(request.POST or None, request.FILES or None, instance=instance)
    # phone_form = InstitutionPhonesFromSet(request.POST or None, request.FILES or None, instance=instance)
    action_form = InstitutionActionFromSet(request.POST or None, request.FILES or None, instance=instance)
    # import ipdb;ipdb.set_trace()
    if form.is_valid() and service_form.is_valid() and doctor_form.is_valid() and photo_form.is_valid() \
            and action_form.is_valid():

        tokens = map(lambda x: x.token, PushUp.objects.filter(institution_id=institution_id))

        obj = form.save()
        if obj and obj.is_active:
            if not instance_is_active:
                data = {
                    'message': u'Добавлено заведение',
                    'obj': obj.name,
                    'obj_id': obj.id,
                    'message_two': u''
                }
                send_pushes_for_topic(data=data)
            else:
                data = {
                    'message': u'В заведении',
                    'obj': obj.name,
                    'obj_id': obj.id,
                    'message_two': u'изменения'
                }
                send_pushes_for_tokens(tokens=tokens, data=data)

        service_form.instance = form.save()
        service = service_form.save()

        # Обработана новая услуга в открытой больничке и до исправлений она тоже была открыта
        if service and obj.is_active and instance_is_active:
            data = {
                'message': u'В',
                'obj': service[0].rel_institution.name,
                'obj_id': service[0].rel_institution.id,
                'message_two': u'новая услуга'
            }
            send_pushes_for_tokens(tokens=tokens, data=data)

        photo_form.instance = form.save()
        photo_form.save()

        doctor_form.instance = form.save()
        doctor_form.save()

        # phone_form.instance = form.save()
        # phone_form.save()

        action_form.instance = form.save()
        action = action_form.save()

        if action and obj.is_active:
            data = {
                'message': u'В',
                'obj': action[0].institution.name,
                'obj_id': action[0].institution.id,
                'message_two': u'новая акция'
            }

            send_pushes_for_topic(data=data)

        if obj.farm:
            return HttpResponseRedirect('/ru/upload/%s/' % obj.pk)
        else:
            return redirect('base:institutions_list')

    return render(request, template,
                  {'form': form, 'service_form': service_form, 'photo_form': photo_form, 'doctor_form': doctor_form,
                   'action_form': action_form})


@admin_required
@user_passes_test(lambda u: u.is_superuser)
def institution_delete(request, institution_id):
    instance = get_object_or_404(Institution, id=institution_id)
    instance.delete()

    return redirect('base:institutions_list')


@admin_required
@user_passes_test(lambda u: u.is_superuser)
def category_create(request):
    context = {}
    context['form'] = CategoryForm(request.POST or None, request.FILES or None)
    if context['form'].is_valid():
        category = context['form'].save(commit=False)
        category.save()
        context['form'] = CategoryForm
        return redirect('base:categories_list')
    return render(request, 'base/create_category.html', context)


@admin_required
@user_passes_test(lambda u: u.is_superuser)
def category_update(request, category_id, template='base/update_category.html'):
    instance = get_object_or_404(Category, pk=category_id)

    form = CategoryForm(request.POST or None, request.FILES or None, instance=instance)

    if form.is_valid():
        form.save()
        return redirect('base:categories_list')

    return render(request, template, {'form': form})


@admin_required
@user_passes_test(lambda u: u.is_superuser)
def category_delete(request, category_id):
    instance = get_object_or_404(Category, id=category_id)
    instance.delete()
    return redirect('base:categories_list')


@admin_required
@user_passes_test(lambda u: u.is_superuser)
def comment_create(request):
    context = {}
    context['form'] = CommentForm(request.POST or None)
    if request.POST and context['form'].is_valid():
        comment = context['form'].save(commit=False)
        comment.save()
        context['form'] = CommentForm
        return redirect('base:comments_list')
    return render(request, 'base/create_comment.html', context)


@admin_required
@user_passes_test(lambda u: u.is_superuser)
def comment_update(request, comment_id, template='base/update_comment.html'):
    instance = get_object_or_404(Comment, id=comment_id)
    form = CommentForm(request.POST or None, instance=instance)
    if form.is_valid():
        comment = form.save()

        if comment and comment.public:
            tokens = map(lambda x: x.token, PushUp.objects.filter(institution_id=comment.institution_id))

            data = {
                'message': u'Новый отзыв к заведению',
                'obj': comment.institution.name,
                'obj_id': comment.institution_id,
                'message_two': u''
            }
            send_pushes_for_tokens(tokens=tokens, data=data)

    return render(request, template, {'form': form})


@admin_required
@user_passes_test(lambda u: u.is_superuser)
def comment_delete(request, comment_id):
    instance = Comment.objects.get(id=comment_id)
    instance.delete()
    return redirect('base:comments_list')


@admin_required
@user_passes_test(lambda u: u.is_superuser)
def institutions_list(request, template='base/institutions.html'):
    institutions = Institution.objects.all()
    paginator = Paginator(institutions, 25)
    page = request.GET.get('page')
    try:
        institutions = paginator.page(page)
    except PageNotAnInteger:
        institutions = paginator.page(1)
    except EmptyPage:
        institutions = paginator.page(paginator.num_pages)
    full_name = request.user.username

    return render(request, template, locals())


@admin_required
@user_passes_test(lambda u: u.is_superuser)
def categories_list(request, template='base/categories.html'):
    categories = Category.objects.all()
    full_name = request.user.username

    return render(request, template, locals())


@admin_required
@user_passes_test(lambda u: u.is_superuser)
def comments_list(request, template='base/comments.html'):
    comments = Comment.objects.all()
    full_name = request.user.username

    return render(request, template, locals())


@admin_required
@user_passes_test(lambda u: u.is_superuser)
def feedback(request):
    context = {}
    context['form'] = FeedbackForm(request.POST or None)
    if context['form'].is_valid():
        feedback = context['form'].save(commit=False)
        feedback.save()
        context['ok'] = True
        context['form'] = FeedbackForm
    else:
        context['ok'] = False
    return render(request, 'base/feedback.html', context)


@admin_required
@user_passes_test(lambda u: u.is_superuser)
def feedback_list(request):
    context = {}
    context['feedbacks'] = Feedback.objects.all().order_by('-created_at')
    return render(request, 'base/feedback-list.html', context)


@admin_required
@user_passes_test(lambda u: u.is_superuser)
def feedback_delete(request, id):
    context = {}
    context['feedback'] = get_object_or_404(Feedback, id=id)
    context['feedback'].delete()
    return redirect('base:feedback_list')


@admin_required
@user_passes_test(lambda u: u.is_superuser)
def about_project(request):
    context = {}
    context['informations'] = AboutProject.objects.all()
    return render(request, 'base/information.html', context)


@admin_required
@user_passes_test(lambda u: u.is_superuser)
def information_create(request):
    context = {}
    context['form'] = AboutProjectForm(request.POST or None)
    if context['form'].is_valid():
        information = context['form'].save(commit=False)
        information.save()
        context['ok'] = True
        context['form'] = AboutProjectForm
        return redirect('base:about_project')
    else:
        context['ok'] = False
    return render(request, 'base/information-form.html', context)


@admin_required
@user_passes_test(lambda u: u.is_superuser)
def information_update(request, id):
    context = {}
    instance = get_object_or_404(AboutProject, id=id)
    context['form'] = AboutProjectForm(request.POST or None, instance=instance)
    if context['form'].is_valid():
        context['form'].save()
        return redirect('base:about_project')
    return render(request, 'base/information-form.html', context)


@admin_required
@user_passes_test(lambda u: u.is_superuser)
def contacts(request):
    context = {}
    context['contacts'] = Contact.objects.all()
    return render(request, 'base/contacts.html', context)


@admin_required
@user_passes_test(lambda u: u.is_superuser)
def contact_create(request):
    context = {}
    context['form'] = ContactForm(request.POST or None)
    if context['form'].is_valid():
        contact = context['form'].save(commit=False)
        contact.save()
        context['ok'] = True
        context['form'] = ContactForm
        return redirect('base:contacts')
    else:
        context['ok'] = False
    return render(request, 'base/contacts-form.html', context)


@admin_required
@user_passes_test(lambda u: u.is_superuser)
def contact_update(request, id):
    context = {}
    instance = get_object_or_404(Contact, id=id)
    context['form'] = ContactForm(request.POST or None, instance=instance)
    if context['form'].is_valid():
        context['form'].save()
        return redirect('base:contacts')
    return render(request, 'base/contacts-form.html', context)


@admin_required
@user_passes_test(lambda u: u.is_superuser)
def contact_delete(request, id):
    context = {}
    context['contact'] = get_object_or_404(Contact, id=id)
    context['contact'].delete()
    return redirect('base:contacts')


@admin_required
@user_passes_test(lambda u: u.is_superuser)
def actions(request):
    context = {}
    context['actions'] = Action.objects.all()
    return render(request, 'base/actions-list.html', context)


@admin_required
@user_passes_test(lambda u: u.is_superuser)
def action_create(request):
    context = {}
    context['form'] = ActionForm(request.POST or None, request.FILES or None)
    if context['form'].is_valid():
        contact = context['form'].save(commit=False)
        contact.save()
        context['ok'] = True
        context['form'] = ActionForm
        return redirect('base:actions')
    else:
        context['ok'] = False
    return render(request, 'base/actions-form.html', context)


@admin_required
@user_passes_test(lambda u: u.is_superuser)
def action_update(request, id):
    context = {}
    instance = get_object_or_404(Action, id=id)
    context['form'] = ActionForm(request.POST or None, instance=instance)
    if context['form'].is_valid():
        context['form'].save()
        return redirect('base:actions')
    return render(request, 'base/actions-form.html', context)


@admin_required
@user_passes_test(lambda u: u.is_superuser)
def action_delete(request, id):
    context = {}
    context['action'] = get_object_or_404(Action, id=id)
    context['action'].delete()
    return redirect('base:actions')


@admin_required
@user_passes_test(lambda u: u.is_superuser)
def doctor_list(request):
    doctors = Doctor.objects.all()
    paginator = Paginator(doctors, 25) # Show 25 contacts per page
    page = request.GET.get('page')
    try:
        doctors = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        doctors = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        doctors = paginator.page(paginator.num_pages)
    return render(request, 'base/doctor_list.html', locals())


@admin_required
@user_passes_test(lambda u: u.is_superuser)
def doctor_create(request):
    context = {}
    context['form'] = DoctorsForm(request.POST or None, request.FILES or None)
    if context['form'].is_valid():
        doctor = context['form'].save()
        doctor.save()
        context['form'] = DoctorsForm
        return redirect('base:doctor_list')
    return render(request, 'base/doctor-form.html', context)


@admin_required
@user_passes_test(lambda u: u.is_superuser)
def doctor_update(request, id):
    instance = get_object_or_404(Doctor, id=id)
    form = DoctorForm(request.POST or None, request.FILES or None, instance=instance)
    if form.is_valid():
        form.save()
        return redirect('base:doctor_list')
    return render(request, 'base/doctor-form.html', locals())


@admin_required
@user_passes_test(lambda u: u.is_superuser)
def doctor_delete(request, id):
    doctor = get_object_or_404(Doctor, id=id)
    doctor.delete()
    return redirect('base:doctor_list')


@admin_required
@user_passes_test(lambda u: u.is_superuser)
def specialty_list(request):
    specialties = Specialty.objects.all()
    return render(request, 'base/specialty_list.html', locals())


@admin_required
@user_passes_test(lambda u: u.is_superuser)
def specialty_create(request):
    form = SpecialtyForm(request.POST or None, request.FILES or None)
    if form.is_valid():
        form.save()
        return redirect('base:specialty_list')
    return render(request, 'base/specialty-form.html', locals())


@admin_required
@user_passes_test(lambda u: u.is_superuser)
def specialty_update(request, id):
    instance = get_object_or_404(Specialty, id=id)
    form = SpecialtyForm(request.POST or None, instance=instance)
    if form.is_valid():
        form.save()
        return redirect('base:specialty_list')
    return render(request, 'base/specialty-form.html', locals())


@admin_required
@user_passes_test(lambda u: u.is_superuser)
def specialty_delete(request, id):
    specialty = get_object_or_404(Specialty, id=id)
    specialty.delete()
    return redirect('base:specialty_list')


@admin_required
@user_passes_test(lambda u: u.is_superuser)
def banner_list(request):
    banners = Banner.objects.all()
    return render(request, 'base/banners.html', locals())


@admin_required
@user_passes_test(lambda u: u.is_superuser)
def banner_create(request):
    form = BannerForm(request.POST or None, request.FILES or None)
    if form.is_valid():
        form.save()
        return redirect('base:banner_list')
    else:
        form = BannerForm()
    return render(request, 'base/banner-form.html', locals())


@admin_required
@user_passes_test(lambda u: u.is_superuser)
def banner_update(request, id):
    instance = Banner.objects.get(id=id)
    form = BannerForm(request.POST or None, instance=instance)
    if form.is_valid():
        form.save()
        return redirect('base:banner_list')
    return render(request, 'base/banner-update-form.html', locals())


@admin_required
@user_passes_test(lambda u: u.is_superuser)
def banner_delete(request, id):
    banner = get_object_or_404(Banner, id=id)
    banner.delete()
    return redirect('base:banner_list')


from django.shortcuts import render_to_response

from django.template import RequestContext
from medicine.models import Price


@admin_required
def import_data(request, ggwp):
    farm = Institution.objects.get(pk=ggwp, farm=True)
    if request.POST:
        price_form = UploadFileForm(request.POST, request.FILES)
    else:
        price_form = UploadFileForm()

    if price_form.is_valid():
        for record in request.FILES['file'].get_records():
            record['institution'] = farm
            Price.objects.create(**record)
        return redirect('/institutions_list/')
    return render_to_response('base/upload_form.html', {'price_form': price_form, 'farm': farm},
                              context_instance=RequestContext(request))


@admin_required
def search_form(request):
    return render_to_response('base/search_form.html')


@admin_required
def search(request):
    if request.GET.get('q'):
        q = request.GET.get('q')
        prices = Price.objects.filter(title__icontains=q)
        return render_to_response('base/search_results.html',
                                  {'prices': prices, 'query': q})
    else:
        return HttpResponse('Please submit a search term.')


@admin_required
def institution_updat(request, institution_id, template='base/institution-form.html'):
    instance = get_object_or_404(Institution, id=institution_id, user=request.user)
    form = InstitutionForm(request.POST or None, request.FILES or None, instance=instance)
    service_form = InstitutionServicesFromSet(request.POST or None, request.FILES or None, instance=instance)
    photo_form = InstitutionPhotosFromSet(request.POST or None, request.FILES or None, instance=instance)
    doctor_form = InstitutionDoctorsFromSet(request.POST or None, request.FILES or None, instance=instance)
    # phone_form = InstitutionPhonesFromSet(request.POST or None, request.FILES or None, instance=instance)
    action_form = InstitutionActionFromSet(request.POST or None, request.FILES or None, instance=instance)
    if form.is_valid() and service_form.is_valid() and doctor_form.is_valid() and photo_form.is_valid() \
            and action_form.is_valid():
        form.save()
        service_form.instance = form.save()
        service_form.save()
        photo_form.instance = form.save()
        photo_form.save()
        doctor_form.instance = form.save()
        doctor_form.save()
        # phone_form.instance = form.save()
        # phone_form.save()
        action_form.instance = form.save()
        action_form.save()
        return redirect('base:institutions')
    return render(request, template,
                  {'form': form, 'service_form': service_form, 'photo_form': photo_form, 'doctor_form': doctor_form,
                   'action_form': action_form})


@admin_required
def institutions(request, template='base/institutions_list.html'):
    institutions = Institution.objects.filter(user=request.user.id)

    return render(request, template, {'institutions': institutions})


@admin_required
def comments(request, template='base/comments_list.html'):
    user = request.user.id
    institution = Institution.objects.filter(user=user)
    comments = Comment.objects.filter(institution=institution, public=True)
    full_name = request.user.username

    return render(request, template, locals())


@admin_required
def upd_comment(request, id):
    context = {}
    user = request.user.id
    institution = Institution.objects.filter(user=user)
    instance = get_object_or_404(Comment, id=id, institution=institution)
    context['form'] = CommentForm(request.POST or None, instance=instance)
    if context['form'].is_valid():
        context['form'].save()
        return redirect('base:comments')
    return render(request, 'base/update_comment.html', context)


@admin_required
def services(request, template='base/services_list.html'):
    user = request.user.id
    rel_institution = Institution.objects.filter(user=user)
    services = Service.objects.filter(rel_institution=rel_institution)
    full_name = request.user.username

    return render(request, template, locals())


@admin_required
def service_upd(request, id):
    context = {}
    user = request.user.id
    rel_institution = Institution.objects.filter(user=user)
    instance = get_object_or_404(Service, id=id, rel_institution=rel_institution)
    context['form'] = ServiceForm(request.POST or None, instance=instance)
    if context['form'].is_valid():
        context['form'].save()
        return redirect('base:services')
    return render(request, 'base/update_service.html', context)


@admin_required
def acttions_list(request, template='base/action_list.html'):
    user = request.user.id
    institution = Institution.objects.filter(user=user)
    actions = Action.objects.filter(institution=institution)
    full_name = request.user.username

    return render(request, template, locals())


@admin_required
def action_upd(request, id):
    context = {}
    user = request.user.id
    institution = Institution.objects.filter(user=user)
    instance = get_object_or_404(Action, id=id, institution=institution)
    context['form'] = ActionForm(request.POST or None, instance=instance)
    if context['form'].is_valid():
        context['form'].save()
        return redirect('base:acttions_list')
    return render(request, 'base/actions-form.html', context)
