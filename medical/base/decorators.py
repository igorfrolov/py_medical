# -*- coding: utf-8 -*-
from django.shortcuts import redirect
from functools import wraps


def admin_required(function):
    """ Unless user is admin redirects with according message """
    def renderer(request, *args, **kwargs):
        @wraps(function)
        def wrapper(request, *args, **kwargs):
            user = request.user

            if user.is_staff or user.is_superuser:

                return function(request, *args, **kwargs)
            return redirect('base:login')
        return wrapper(request, *args, **kwargs)
    return renderer